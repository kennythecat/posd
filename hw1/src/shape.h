#ifndef SHAPE_H
#define SHAPE_H

#include <math.h>
#include <iostream>
#include <list>
#include <iomanip>


using namespace std;

class Shape {
public:
    virtual double area() const { return 0.0; }
    virtual double perimeter() const = 0;
    virtual std::string info() const = 0;
    virtual void addShape(Shape* shape) 
    { 
        string error="Can't add.";
        throw error;
    }
    virtual void deleteShape(Shape* shape) 
    { 
        string error="Can't delete.";
        throw error;
    }
};

#endif