#include "../src/rectangle.h"

TEST(CaseRectangle, Creation)
{
    try
    {
        Rectangle r(0.0, -1.0);
    }
    catch (std::string s)
    {
        std::cerr << s << '\n';
    }
    ASSERT_ANY_THROW(Rectangle r(10.0, -5.00));
}
TEST(CaseRectangle, LengthShouldGreaterThanZero)
{
    ASSERT_ANY_THROW(Rectangle r(1.0, 0.00));
}
TEST(CaseRectangle, LengthShouldNotBeNegative)
{
    ASSERT_ANY_THROW(Rectangle r(-1.0, 1.00));
}
TEST(CaseRectangle, WidthShouldGreaterThanZero)
{

    ASSERT_ANY_THROW(Rectangle r(0.0, 1.00));
}
TEST(CaseRectangle, WidthShouldNotBeNegative)
{

    ASSERT_ANY_THROW(Rectangle r(1.0, -1.00));
}
TEST(CaseRectangle, NoException)
{
    ASSERT_NO_THROW(Rectangle r(10.0, 5.00));
}

TEST(CaseRectangle, Area)
{
    Rectangle r(10.0, 5.00);

    ASSERT_NEAR(50, r.area(), 0.0001);
}

TEST(CaseRectangle, Perimter)
{
    Rectangle r(10.0, 5.00);
    ASSERT_NEAR(30, r.perimeter(), 0.001);
}

TEST(CaseRectangle, Info)
{
    Rectangle r(10.0, 5.00);
    ASSERT_STRCASEEQ("Rectangle (10.00 5.00)", (r.info()).c_str());
}

