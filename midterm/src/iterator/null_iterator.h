#pragma once

#include "iterator.h"

class NullIterator : public Iterator {
    public:
        void first() override {
            throw "I don't know";
        }

        Article* currentItem() const override {
            throw "I don't know";

        }

        void next() override {
            throw "I don't know";
        }

        bool isDone() const override {
            return true;
        }
};
