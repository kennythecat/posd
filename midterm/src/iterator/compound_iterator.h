#pragma once

#include "iterator.h"

template<class ForwardIterator> 

class CompoundIterator : public Iterator {
    public:
        CompoundIterator(ForwardIterator begin, ForwardIterator end) {
            _begin = begin;
            _current = _begin;
            _end = end;
        }

        void first() override {
            _current =  _begin;
        }

        Article* currentItem() const override {
            if(_current == _end){
                throw "current is end";
            }
            return *_current;
        }

        void next() override {
            if(_current == _end){
                throw "over size";
            }
            _current++;
        }

        bool isDone() const override {
            if (_current == _end){
                return true;
            }
            else{
                return false;
            }
        }
    private:
        ForwardIterator _begin;
        ForwardIterator _current;
        ForwardIterator _end;
};
