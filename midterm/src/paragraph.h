#pragma once

#include <string>
#include<list>

#include "./article.h"
#include "./iterator/compound_iterator.h"
#include "./visitor/article_visitor.h"

class Paragraph : public Article {
    public:
        Paragraph(int level, std::string text) {
            if(level > 6 ||level<1)
                throw "error";
            _level = level;
            _text = text;
            
        }

        ~Paragraph() {
            // delete _iterator;
        }

        std::string getText() const override {
            return _text;
        }

        int getLevel() const override {
            return _level;
        }

        Iterator* createIterator() override {
            return new CompoundIterator<std::list<Article*>::iterator>(_listItem->begin(), _listItem->end());

        }

        void add(Article* content) override {
            if(content->getLevel() != 0 && content->getLevel() <= _level){
                throw "Can't add lower or equal level Paragraph";
            }
            _listItem->push_back(content);
        }

        void accept(ArticleVisitor* visitor) override {
            visitor->visitParagraph(this);
        }
    private:
        std::string _text;
        int _level;
        Iterator* _iterator;
        std::list<Article*>* _listItem = new std::list<Article*>;

};
