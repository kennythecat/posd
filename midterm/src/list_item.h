#pragma once

#include <string>

#include "./article.h"
#include "./visitor/article_visitor.h"
#include "./iterator/null_iterator.h"

class ListItem : public Article {
    public:
        ListItem(std::string text) {
            _text = text;
        }

        std::string getText() const override {
            return _text;
        }

        Iterator* createIterator() override {
            _iterator = new NullIterator();
            return _iterator;
        }

        void accept(ArticleVisitor* visitor) override {
            visitor->visitListItem(this);
        }
    private:
        std::string _text;
        Iterator* _iterator;
};
