#pragma once

#include <string>
#include "article_visitor.h"
#include "../text.h"
#include "../list_item.h"
#include "../paragraph.h"
#include "../iterator/iterator.h"

class HtmlVisitor : public ArticleVisitor {
    public:
        HtmlVisitor(){
            _result = "";
        }
                
        void visitListItem(ListItem* li) override{ 
            _result += "<li>"+li->getText()+"</li>";

        };

        void visitText(Text* t) override{ 
            _result += "<span>"+t->getText()+"</span>";
        };

        void visitParagraph(Paragraph* p) override{ 
            _result += "<div>";
            std::string count = std::to_string(p->getLevel());
            _result += "<h"+count+">"+p->getText()+"</h"+count+">";
 
            Iterator* it = p->createIterator();
            for (it->first(); !it->isDone(); it->next()) {
                it->currentItem()->accept(this);
            }
            
            _result += "</div>";
        };

        std::string getResult() const override { 
            return _result;
        }
    private:
        std::string _result;
};
