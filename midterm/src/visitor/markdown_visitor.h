#pragma once

#include <string>
#include "article_visitor.h"
#include "../text.h"
#include "../list_item.h"
#include "../paragraph.h"

class MarkdownVisitor : public ArticleVisitor {
    public:
        MarkdownVisitor(){
            _result = "";
        }
    
        void visitListItem(ListItem* li) override{
            _result += "- "+li->getText()+"\n";
        };

        void visitText(Text* t) override{
            _result += t->getText()+"\n";
        };

        void visitParagraph(Paragraph* p) override{
            int count = p->getLevel();
            for(int i =0;i<count;i++){
                _result += "#";
            }
            _result += " "+p->getText()+"\n";
            Iterator* it = p->createIterator();
            for (it->first(); !it->isDone(); it->next()) {
                it->currentItem()->accept(this);
            }


        };

        std::string getResult() const override {
            return _result;
        }
    private:
    std::string _result;

};

