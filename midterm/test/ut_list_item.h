#pragma once

#include <string>

#include "../src/visitor/article_visitor.h"
#include "../src/article.h"
#include "../src/visitor/html_visitor.h"
#include "../src/visitor/markdown_visitor.h"
#include "../src/list_item.h"
#include "../src/iterator/iterator.h"

TEST(CaseListItem, Init){
    ListItem listItem("123");
    EXPECT_EQ("123", listItem.getText());
}

TEST(CaseListItem, GetLevel){
    ListItem listItem("123");
    EXPECT_EQ(0, listItem.getLevel());
}

TEST(CaseListItem, Add){
    ListItem listItem("123");
    ListItem listItem2("456");
    ASSERT_ANY_THROW(listItem.add(&listItem2));
}

TEST(CaseListItem, iterator){
    ListItem listItem("123");
    Iterator* iterator = listItem.createIterator();
    ASSERT_TRUE(iterator->isDone());
    ASSERT_ANY_THROW(iterator->currentItem());
    ASSERT_ANY_THROW(iterator->first());
    ASSERT_ANY_THROW(iterator->next());

}

TEST(CaseListItem, acceptHtmlVisitor){
    ListItem* listItem = new ListItem("123");
    HtmlVisitor* hv = new HtmlVisitor();
    listItem->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<li>123</li>",result);
}

TEST(CaseListItem, acceptMarkdownVisitor){
    ListItem* listItem = new ListItem("123");
    MarkdownVisitor* mv = new MarkdownVisitor();
    listItem->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("- 123\n",result);
}
