#pragma once

#include <string>

#include "../../src/visitor/article_visitor.h"
#include "../../src/article.h"
#include "../../src/text.h"
#include "../../src/list_item.h"
#include "../../src/visitor/html_visitor.h"
#include "../../src/paragraph.h"
#include "../../src/iterator/iterator.h"

TEST(CaseHtmlVisitor, text){
    Text* text = new Text("123");
    HtmlVisitor* hv = new HtmlVisitor();
    text->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<span>123</span>",result);
}

TEST(CaseHtmlVisitor, ListItem){
    ListItem* listItem = new ListItem("123");
    HtmlVisitor* hv = new HtmlVisitor();
    listItem->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<li>123</li>",result);
}
TEST(CaseHtmlVisitor, paragraph){
    Paragraph* paragraph = new Paragraph(2,"123");
    HtmlVisitor* hv = new HtmlVisitor();
    paragraph->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<div><h2>123</h2></div>",result);
}
TEST(CaseHtmlVisitor, multi){
    Paragraph* p = new Paragraph(1, "title");
    p->add(new ListItem("list1"));
    p->add(new ListItem("list2"));
    p->add(new Text("text"));
    Paragraph* p2 = new Paragraph(2, "title2");
    p2->add(new ListItem("list3"));
    p2->add(new ListItem("list4"));
    p2->add(new Text("sub text"));
    p->add(p2);
    HtmlVisitor* hv = new HtmlVisitor();
    p->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<div><h1>title</h1><li>list1</li><li>list2</li><span>text</span><div><h2>title2</h2><li>list3</li><li>list4</li><span>sub text</span></div></div>",result);


}