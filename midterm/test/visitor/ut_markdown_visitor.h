#pragma once

#include <string>

#include "../../src/visitor/article_visitor.h"
#include "../../src/article.h"
#include "../../src/text.h"
#include "../../src/list_item.h"
#include "../../src/visitor/markdown_visitor.h"
#include "../../src/paragraph.h"
#include "../../src/iterator/iterator.h"

TEST(CaseMarkdownVisitor, text){
    Text* text = new Text("1  23");
    MarkdownVisitor* mv = new MarkdownVisitor();
    text->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("1  23\n",result);

}
TEST(CaseMarkdownVisitor, listItem){
    ListItem* listItem = new ListItem("123");
    MarkdownVisitor* mv = new MarkdownVisitor();
    listItem->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("- 123\n",result);

}
TEST(CaseMarkdownVisitor, paragraph){
    Paragraph* paragraph = new Paragraph(2,"123");
    MarkdownVisitor* mv = new MarkdownVisitor();
    paragraph->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("## 123\n",result);
}
TEST(CaseMarkdownVisitor, multi){
    Paragraph* p= new Paragraph(1, "title");
    p->add(new ListItem("list1"));
    p->add(new ListItem("list2"));
    p->add(new Text("text"));
    Paragraph* p2 = new Paragraph(2, "title2");
    p2->add(new ListItem("list3"));
    p2->add(new ListItem("list4"));
    p2->add(new Text("sub text"));
    p->add(p2);
    MarkdownVisitor* mv = new MarkdownVisitor();
    p->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("# title\n- list1\n- list2\ntext\n## title2\n- list3\n- list4\nsub text\n",result);
}