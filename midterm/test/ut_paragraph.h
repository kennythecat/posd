#pragma once

#include <string>

#include "../src/visitor/article_visitor.h"
#include "../src/article.h"
#include "../src/text.h"
#include "../src/list_item.h"
#include "../src/visitor/html_visitor.h"
#include "../src/visitor/markdown_visitor.h"
#include "../src/paragraph.h"
#include "../src/iterator/iterator.h"

TEST(CaseParagraph, Init){
    Paragraph paragraph(5,"123");
    EXPECT_EQ("123", paragraph.getText());
    ASSERT_ANY_THROW(new Paragraph (7,"123"));
    ASSERT_ANY_THROW(new Paragraph (-1,"123"));

}

TEST(CaseParagraph, GetLevel){
    Paragraph paragraph(5,"123");
    EXPECT_EQ(5, paragraph.getLevel());
}

TEST(CaseParagraph, GetText){
    Paragraph paragraph(5,"123");
    EXPECT_EQ("123", paragraph.getText());
}

TEST(CaseParagraph, Add){
    Paragraph paragraph(3,"123");
    Paragraph paragraph2(1,"456");
    ASSERT_ANY_THROW(paragraph.add(&paragraph2));
    paragraph.add(new Text("123"));
    paragraph.add(new ListItem("123"));

}

TEST(CaseParagraph, acceptHtmlVisitor){
    Paragraph* paragraph = new Paragraph(2,"123");
    HtmlVisitor* hv = new HtmlVisitor();
    paragraph->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<div><h2>123</h2></div>",result);
}

TEST(CaseParagraph, acceptMarkdownVisitor){
    Paragraph* paragraph = new Paragraph(2,"123");
    MarkdownVisitor* mv = new MarkdownVisitor();
    paragraph->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("## 123\n",result);
}