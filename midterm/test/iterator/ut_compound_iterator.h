#pragma once

#include <string>

#include "../../src/article.h"
#include "../../src/paragraph.h"
#include "../../src/text.h"
#include "../../src/list_item.h"
#include "../../src/iterator/iterator.h"
#include "../../src/iterator/compound_iterator.h"



TEST(CaseCompoundIterator, compoundIteratorInit){
    std::list<Article*> _articles;
    Iterator* it = new CompoundIterator<std::list<Article*>::iterator>(_articles.begin(),_articles.end() );
    ASSERT_TRUE(typeid(it) == typeid(Iterator*));
    delete it;
}

TEST(CaseCompoundIterator, compoundIteratorPlace){
    Article* text = new Text("1.5");
    Article* text2 = new Text("45.2");
    Paragraph* paragraph = new Paragraph(1,"123");    
    paragraph->add(text);
    paragraph->add(text2);
    Iterator* it = paragraph->createIterator();
    ASSERT_EQ(it->currentItem(),text);
    it->next();
    ASSERT_EQ(it->currentItem(), text2);
    it->first();
    ASSERT_EQ(it->currentItem(), text);
    delete text2;
    delete it;
    delete paragraph;
    delete text;
}

TEST(CaseCompoundIterator, compoundIteratorDone){
    Article* text = new Text("1.5");
    Article* text2 = new Text("45.2");
    Paragraph* paragraph = new Paragraph(1,"123");    
    paragraph->add(text);
    paragraph->add(text2);
    Iterator* it = paragraph->createIterator();

    ASSERT_FALSE(it->isDone());
    it->next();
    ASSERT_FALSE(it->isDone());
    it->next();
    ASSERT_TRUE(it->isDone());
    ASSERT_ANY_THROW(it->next());
    delete text2;
    delete it;
    delete paragraph;
    delete text;

}

