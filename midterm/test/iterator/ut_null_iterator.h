#pragma once

#include <string>

#include "../../src/iterator/iterator.h"
#include "../../src/iterator/null_iterator.h"

TEST(CaseIterator, NullIteratorFirst){
    NullIterator newIterator;
    ASSERT_ANY_THROW(newIterator.first());
}

TEST(CaseIterator, NullIteratorCurrentItem){
    NullIterator newIterator;
    ASSERT_ANY_THROW(newIterator.currentItem());
}

TEST(CaseIterator, NullIteratorNext){
    NullIterator newIterator;

    try{
        newIterator.next();
        FAIL();
    }
    catch(const char* msg){
        ASSERT_EQ(msg,"I don't know");
    }
    
    ASSERT_ANY_THROW(newIterator.first());
}

TEST(CaseIterator, NullIteratorIsDone){
    NullIterator newIterator;
    ASSERT_TRUE(newIterator.isDone());
}

