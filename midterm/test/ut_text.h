#pragma once

#include <string>

#include "../src/article.h"
#include "../src/visitor/article_visitor.h"
#include "../src/visitor/html_visitor.h"
#include "../src/visitor/markdown_visitor.h"

#include "../src/text.h"
#include "../src/iterator/iterator.h"

TEST(CaseText, Init){
    Text text("123");
    EXPECT_EQ("123", text.getText());
}

TEST(CaseText, GetLevel){
    Text text("123");
    EXPECT_EQ(0, text.getLevel());
}

TEST(CaseText, Add){
    Text text("123");
    Text text2("456");
    ASSERT_ANY_THROW(text.add(&text2));
}

TEST(CaseText, iterator){
    Text text("123");
    Iterator* iterator = text.createIterator();
    ASSERT_TRUE(iterator->isDone());
    ASSERT_ANY_THROW(iterator->currentItem());
    ASSERT_ANY_THROW(iterator->first());
    ASSERT_ANY_THROW(iterator->next());

}

TEST(CaseText, acceptHtmlVisitor){
    Text* text = new Text("123");
    HtmlVisitor* hv = new HtmlVisitor();
    text->accept(hv);
    std::string result = hv->getResult();
    EXPECT_EQ("<span>123</span>",result);
}
TEST(CaseText, acceptMarkdownVisitor){
    Text* text = new Text("1  23");
    MarkdownVisitor* mv = new MarkdownVisitor();
    text->accept(mv);
    std::string result = mv->getResult();
    EXPECT_EQ("1  23\n",result);
}