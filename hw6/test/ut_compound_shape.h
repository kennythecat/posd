#include "../src/compound_shape.h"
#include "../src/visitor/shape_info_visitor.h"
TEST(CaseCompoundShape, Accept)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Circle *c2 = new Circle(12.3456);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    CompoundShape *cs2 = new CompoundShape();
    cs2->addShape(c2);
    cs2->addShape(cs1);
    ShapeInfoVisitor visitor;
    cs2->accept(&visitor);
    ASSERT_EQ("CompoundShape {\n  Circle (12.35)\n  CompoundShape {\n    Circle (1.10)\n    Rectangle (3.14 4.00)\n  }\n}\n", visitor.getResult());
    delete c1, c2, r1;
}
TEST(CaseCompoundShape, ConstructorNoException)
{
    ASSERT_NO_THROW(CompoundShape *cs1 = new CompoundShape());
}
TEST(CaseCompoundShape, DeleteInnerShapeCorrectly)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.0);
    Circle *c2 = new Circle(1.0);

    Rectangle *r1 = new Rectangle(1.0, 2.0);
    cs1->addShape(c1);
    CompoundShape *cs2 = new CompoundShape();
    cs2->addShape(r1);
    cs2->addShape(cs1);
    // cs2->deleteShape(r1);
    cs2->deleteShape(c1);

    ShapeInfoVisitor visitor;
    cs2->accept(&visitor);
    ASSERT_EQ("CompoundShape {\n  Rectangle (1.00 2.00)\n  CompoundShape {\n  }\n}\n", visitor.getResult());
    delete c1, c2, r1, cs1;
}

TEST(CaseCompoundShape, CompoundShapeShouldBeAShape)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.0);
    cs1->addShape(c1);

    ASSERT_EQ("CompoundShape", cs1->info());
    delete cs1, c1;
}
TEST(CaseCompoundShape, AreaShouldBeCalculatedCorrectly)
{
    CompoundShape *cs1 = new CompoundShape();
    Rectangle *r1 = new Rectangle(2, 2);
    cs1->addShape(r1);

    ASSERT_EQ(4, cs1->area());
    delete cs1, r1;
}
TEST(CaseCompoundShape, PerimeterShouldBeCalculatedCorrectly)
{
    CompoundShape *cs1 = new CompoundShape();
    Rectangle *r1 = new Rectangle(2, 2);
    cs1->addShape(r1);

    ASSERT_EQ(8, cs1->perimeter());
    delete cs1, r1;
}
TEST(CaseCompoundShape, InfoShouldBeCorrect)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(10);
    cs1->addShape(c1);

    ASSERT_EQ("CompoundShape", cs1->info());
    delete cs1, c1;
}
TEST(CaseCompoundShape, IteratorNoException)
{
    CompoundShape *cs1 = new CompoundShape();
    Rectangle *r1 = new Rectangle(2, 2);
    cs1->addShape(r1);

    ASSERT_NO_THROW(cs1->createIterator());
    delete cs1, r1;
}
TEST(CaseCompoundShape, AddShapeCorrectly)
{
    CompoundShape *cs1 = new CompoundShape();
    Rectangle *r1 = new Rectangle(2, 2);
    Circle *c1 = new Circle(10);
    cs1->addShape(r1);

    ASSERT_NO_THROW(cs1->addShape(c1));
    delete cs1, r1, c1;
}
TEST(CaseCompoundShape, DeleteShapeCorrectly)
{
    CompoundShape *cs1 = new CompoundShape();
    Rectangle *r1 = new Rectangle(2, 2);
    cs1->addShape(r1);

    ASSERT_NO_THROW(cs1->deleteShape(r1));
    delete cs1, r1;
}
TEST(CaseCompoundShape, DeleteSelectedShapeCorrectly)
{
    CompoundShape *cs = new CompoundShape();
    Circle *c1 = new Circle(1.0);
    Circle *c2 = new Circle(1.0);
    Rectangle *r1 = new Rectangle(5.0, 5.0);
    cs->addShape(c1);
    cs->addShape(c2);
    cs->addShape(r1);
    Iterator *ic1 = cs->createIterator();

    Shape *result = selectShape(cs, [](Shape *shape)
                                { return shape->area() > 20.0 && shape->area() < 30.0; });

    ASSERT_NO_THROW(cs->deleteShape(result));
    delete c1, c2, r1;
}