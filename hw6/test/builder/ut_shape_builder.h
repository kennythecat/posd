#include <string>

#include "../../src/shape.h"
#include "../../src/builder/shape_builder.h"

TEST(CaseBuilder, BuildCircle)
{
    ShapeBuilder *builder = new ShapeBuilder();
    double radius = 1.0;
    builder->buildCircle(radius);
    Shape *result = builder->getShape();
    ASSERT_NEAR(1 * 1 * M_PI, result->area(), 0.001);
    delete builder;
}
TEST(CaseBuilder, Rectangle)
{
    ShapeBuilder *builder = new ShapeBuilder();
    builder->buildRectangle(2, 3);
    Shape *result = builder->getShape();
    ASSERT_NEAR(6, result->area(), 0.001);
    delete builder;
}
TEST(CaseBuilder, BuildTriangle)
{
    ShapeBuilder *builder = new ShapeBuilder();
    builder->buildTriangle(0.0, 2.0, 2.0, 0.0);
    Shape *result = builder->getShape();
    ASSERT_NEAR(2, result->area(), 0.001);
    delete builder;
}
TEST(CaseBuilder, BuildEmptyCompound)
{
    ShapeBuilder *builder = new ShapeBuilder();
    builder->buildCompoundBegin();
    builder->buildCompoundEnd();
    Shape *result = builder->getShape();
    ASSERT_NEAR(0, result->area(), 0.001);
    delete builder;
}
TEST(CaseBuilder, BuildCompound)
{
    ShapeBuilder *builder = new ShapeBuilder();
    builder->buildCompoundBegin();
    builder->buildCircle(1.0);
    builder->buildCircle(2.0);
    builder->buildCompoundEnd();
    Shape *result = builder->getShape();
    ASSERT_NEAR(1 * 1 * M_PI + 2 * 2 * M_PI, result->area(), 0.001);
    delete builder;
}
TEST(CaseBuilder, BuildComplexCompound)
{
    ShapeBuilder *builder = new ShapeBuilder();

    builder->buildCompoundBegin();
    builder->buildCompoundBegin();
    builder->buildCircle(1.0);
    builder->buildCircle(2.0);
    builder->buildCompoundEnd();
    builder->buildCompoundEnd();
    Shape *result = builder->getShape();
    ASSERT_NEAR(1 * 1 * M_PI + 2 * 2 * M_PI, result->createIterator()->currentItem()->area(), 0.001);
    delete builder;
}
