#include "../../src/builder/scanner.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
TEST(CaseScanner, ScanIllegalWordShouldIgnore)
{
    std::string input = "I Circle eee ,tt{t3.14159a";
    Scanner scanner(input);
    std::string first = scanner.next();  // `first` is "Circle"
    std::string second = scanner.next(); // `second` is ","
    std::string third = scanner.next();  // `second` is ","
    double forth = scanner.nextDouble(); // `third` is 3.14159
    ASSERT_EQ("Circle", first);
    ASSERT_EQ(",", second);
    ASSERT_EQ("{", third);
    ASSERT_EQ(3.14159, forth);
}
TEST(CaseScanner, ScanCircleCorrectly)
{
    std::string input = "Circle (1.0) ";
    Scanner scanner(input);
    std::string first = scanner.next(); // `first` is "Circle"
    ASSERT_EQ("Circle", first);
}
TEST(CaseScanner, ScanRectangleCorrectly)
{
    std::string input = "Rectangle (3.14 4.00)";
    Scanner scanner(input);
    std::string first = scanner.next(); // `first` is "Circle"
    ASSERT_EQ("Rectangle", first);
}
TEST(CaseScanner, ScanTriangleCorrectly)
{
    std::string input = "Triangle ([3,0] [0,4])";
    Scanner scanner(input);
    std::string first = scanner.next(); // `first` is "Circle"
    ASSERT_EQ("Triangle", first);
}
TEST(CaseScanner, IsDoneShouldBeCorrect)
{
    std::string input = "Circle ";
    Scanner scanner(input);
    scanner.next();
    ASSERT_ANY_THROW(scanner.next());
}
TEST(CaseScanner, NextShouldThrowExceptionWhenIsDone)
{
    std::string r1 = "Circle (1.0)";
    Scanner scanner(r1);
    scanner.next(); // `first` is "Circle
    scanner.next(); // `first` is "Circle
    scanner.nextDouble();
    scanner.next();                   // `first` is "Circle
    ASSERT_ANY_THROW(scanner.next()); // `first` is "Circle
}
TEST(CaseScanner, NextDoubleShouldThrowExceptionWhenIsDone)
{
    std::string r1 = "Circle (1.0)";
    Scanner scanner(r1);
    scanner.nextDouble();
    ASSERT_ANY_THROW(scanner.nextDouble()); // `first` is "Circle
}
TEST(CaseScanner, ScanEmptyCompoundCorrectly)
{
    std::string input = "CompoundShape { }";
    Scanner scanner(input);
    ASSERT_EQ("CompoundShape", scanner.next());
}
TEST(CaseScanner, ScanSimpleCompoundCorrectly)
{
    std::string input = "CompoundShape {\n  Circle (1.0)\n  Rectangle (3.14 4.00)\n  Triangle ([3,0] [0,4])\n}\n";
    Scanner scanner(input);
    ASSERT_EQ("CompoundShape", scanner.next());
    ASSERT_EQ("{", scanner.next());
    ASSERT_EQ("Circle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("Rectangle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("Triangle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ("[", scanner.next());
    ASSERT_EQ(",", scanner.next());
    ASSERT_EQ("]", scanner.next());
    ASSERT_EQ("[", scanner.next());
    ASSERT_EQ(",", scanner.next());
    ASSERT_EQ("]", scanner.next());
    ASSERT_EQ(")", scanner.next());
}
TEST(CaseScanner, ScanComplexCompoundCorrectly)
{
    std::string input = "CompoundShape {\n  Circle (1.0)\n  Triangle ([3,0] [0,4])\n  CompoundShape {\n    Circle (1.0)\n    Rectangle (3.14 4.00)\n    Triangle ([3,0] [0,4])\n  }\n  Rectangle (3.14 4.00)\n}\n";
    Scanner scanner(input);
    ASSERT_EQ("CompoundShape", scanner.next());
    ASSERT_EQ("{", scanner.next());
    ASSERT_EQ("Circle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("Triangle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ("[", scanner.next());
    ASSERT_EQ(",", scanner.next());
    ASSERT_EQ("]", scanner.next());
    ASSERT_EQ("[", scanner.next());
    ASSERT_EQ(",", scanner.next());
    ASSERT_EQ("]", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("CompoundShape", scanner.next());
    ASSERT_EQ("{", scanner.next());
    ASSERT_EQ("Circle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("Rectangle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("Triangle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ("[", scanner.next());
    ASSERT_EQ(",", scanner.next());
    ASSERT_EQ("]", scanner.next());
    ASSERT_EQ("[", scanner.next());
    ASSERT_EQ(",", scanner.next());
    ASSERT_EQ("]", scanner.next());
    ASSERT_EQ(")", scanner.next());
    ASSERT_EQ("}", scanner.next());
    ASSERT_EQ("Rectangle", scanner.next());
    ASSERT_EQ("(", scanner.next());
    ASSERT_EQ(")", scanner.next());
}
