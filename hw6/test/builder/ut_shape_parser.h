#include "../../src/builder/shape_parser.h"
#include "../../src/builder/scanner.h"
TEST(CaseParser, BuildCircle)
{
    ShapeParser *parser = new ShapeParser("test/data/circle.txt");
    parser->parse();
    ASSERT_NEAR(3.1415926, (parser->getShape())->area(), 0.001);
    delete parser;
}
TEST(CaseParser, BuildRectangle)
{
    ShapeParser *parser = new ShapeParser("test/data/rectangle.txt");
    parser->parse();
    ASSERT_NEAR(12, (parser->getShape())->area(), 0.001);
    delete parser;
}
TEST(CaseParser, BuildTriangle)
{
    ShapeParser *parser = new ShapeParser("test/data/triangle.txt");
    parser->parse();
    ASSERT_NEAR(6, (parser->getShape())->area(), 0.001);
    delete parser;
}
TEST(CaseParser, BuildEmptyCompound)
{
    ShapeParser *parser = new ShapeParser("test/data/empty_compound.txt");
    parser->parse();
    ASSERT_NEAR(0, (parser->getShape())->area(), 0.01);
    delete parser;
}
TEST(CaseParser, BuildSimpleCompound)
{
    ShapeParser *parser = new ShapeParser("test/data/simple_compound.txt");
    parser->parse();
    ASSERT_NEAR(21.701592653589792, (parser->getShape())->area(), 0.01);
}
TEST(CaseParser, BuildComplexCompound)
{
    ShapeParser *parser = new ShapeParser("test/data/complex_compound.txt");
    parser->parse();
    ASSERT_NEAR(43.403185307179584, (parser->getShape())->area(), 0.01);
    delete parser;
}