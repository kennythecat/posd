#include <iostream>
#include "../../src/compound_shape.h"
#include "../../src/visitor/shape_info_visitor.h"
TEST(CaseShapeInfoVisitor, Accept)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Circle *c2 = new Circle(12.3456);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    CompoundShape *cs2 = new CompoundShape();
    cs2->addShape(c2);
    cs2->addShape(cs1);
    ShapeInfoVisitor visitor;
    cs2->accept(&visitor);
    ASSERT_EQ("CompoundShape {\n  Circle (12.35)\n  CompoundShape {\n    Circle (1.10)\n    Rectangle (3.14 4.00)\n  }\n}\n", visitor.getResult());
    delete c1, c2, r1;
}
TEST(CaseShapeInfoVisitor, VisitCircle)
{
    Circle c(10);
    ShapeInfoVisitor visitor;
    visitor.visitCircle(&c);
    ASSERT_EQ("Circle (10.00)\n", visitor.getResult());
}

TEST(CaseShapeInfoVisitor, VisitRectangle)
{
    Rectangle r(2, 3);
    ShapeInfoVisitor visitor;
    visitor.visitRectangle(&r);
    ASSERT_EQ("Rectangle (2.00 3.00)\n", visitor.getResult());
}

TEST(CaseShapeInfoVisitor, VisitTriangle)
{
    Triangle t(TwoDimensionalVector(2, 0), TwoDimensionalVector(0, 2));
    ShapeInfoVisitor visitor;
    visitor.visitTriangle(&t);
    ASSERT_EQ("Triangle ([2.00,0.00] [0.00,2.00])\n", visitor.getResult());
}
TEST(CaseShapeInfoVisitor, VisitCompound)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Circle *c2 = new Circle(12.3456);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);

    CompoundShape *cs2 = new CompoundShape();
    cs2->addShape(c2);
    cs2->addShape(cs1);

    ShapeInfoVisitor visitor;
    cs2->accept(&visitor);
    // cout << typeid(cs1).name();
    // cout << visitor.getResult();

    ASSERT_EQ("CompoundShape {\n  Circle (12.35)\n  CompoundShape {\n    Circle (1.10)\n    Rectangle (3.14 4.00)\n  }\n}\n", visitor.getResult());
    delete c1, c2, r1;
}