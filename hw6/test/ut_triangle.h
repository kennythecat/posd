#include "../src/triangle.h"
#include "../src/two_dimensional_vector.h"
#include "../src/visitor/shape_info_visitor.h"
TEST(CaseTriangle, Accept)
{
    Triangle t(TwoDimensionalVector(0, 2), TwoDimensionalVector(2, 0));
    ShapeInfoVisitor visitor;
    t.accept(&visitor);
    ASSERT_EQ("Triangle ([0.00,2.00] [2.00,0.00])\n", visitor.getResult());
}
TEST(CaseTriangle, Creation)
{
    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(2.0, 0.0);

    ASSERT_ANY_THROW(Triangle t(v1, v2));
}
TEST(CaseTriangle, Creation2)
{
    TwoDimensionalVector v1(0.0, 0.0);
    TwoDimensionalVector v2(2.0, 0.0);

    ASSERT_ANY_THROW(Triangle t(v1, v2));
}
TEST(CaseTriangle, Creation3)
{
    TwoDimensionalVector v1(0.0, 2.0);
    TwoDimensionalVector v2(2.0, 0.0);

    ASSERT_NO_THROW(Triangle t(v1, v2));
}

TEST(CaseTriangle, Area)
{
    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);
    Triangle t(v1, v2);
    ASSERT_NEAR(2, t.area(), 0.001);
    ASSERT_NEAR(4 + 2 * sqrt(2), t.perimeter(), 0.01);
    ASSERT_STRCASEEQ("Triangle ([2.00,0.00] [0.00,2.00])", (t.info()).c_str());
}
TEST(CaseTriangle, Peremeter)
{
    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);
    Triangle t(v1, v2);

    ASSERT_NEAR(4 + 2 * sqrt(2), t.perimeter(), 0.01);
}
TEST(CaseTriangle, Info)
{
    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);
    Triangle t(v1, v2);

    ASSERT_STRCASEEQ("Triangle ([2.00,0.00] [0.00,2.00])", (t.info()).c_str());
}
TEST(CaseTriangle, IsDoneOfIteratorShouldBeTrue)
{
    TwoDimensionalVector v1(2, 0);
    TwoDimensionalVector v2(0, 2);
    Triangle t(v1, v2);
    Iterator *ic1 = t.createIterator();

    ASSERT_TRUE(ic1->isDone());
}
TEST(CaseTriangle, CurrentItemOfIteratorShouldThrowException)
{
    TwoDimensionalVector v1(2, 0);
    TwoDimensionalVector v2(0, 2);
    Triangle t(v1, v2);
    Iterator *ic1 = t.createIterator();

    ASSERT_ANY_THROW(ic1->currentItem());
}
TEST(CaseTriangle, AddShouldThrowException)
{
    TwoDimensionalVector v1(2, 0);
    TwoDimensionalVector v2(0, 2);
    Triangle t(v1, v2);
    Shape *s1;

    ASSERT_ANY_THROW(t.addShape(s1));
}
