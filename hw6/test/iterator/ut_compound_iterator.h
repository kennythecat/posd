#include <gtest/gtest.h>
#include "../../src/circle.h"
#include "../../src/rectangle.h"
#include "../../src/triangle.h"
#include "../../src/two_dimensional_vector.h"
TEST(CaseCompoundIterator, CompoundIteratorIsAIterator)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);

    ASSERT_NO_THROW(Iterator *ic1 = cs1->createIterator());
    delete c1, r1, cs1;
}

TEST(CaseCompoundIterator, FirstNoException)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    Iterator *ic1 = cs1->createIterator();

    ASSERT_NO_THROW(ic1->first());
    delete cs1, c1, r1;
}
TEST(CaseCompoundIterator, CurrentItemIsCorrect)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    Iterator *ic1 = cs1->createIterator();
    string s1 = ic1->currentItem()->info();

    ASSERT_EQ("Circle (1.10)", s1);
    delete cs1, c1, r1;
}
TEST(CaseCompoundIterator, NextNoException)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    Iterator *ic1 = cs1->createIterator();

    ASSERT_NO_THROW(ic1->next());
    delete cs1, c1, r1;
}
TEST(CaseCompoundIterator, IsDoneShouldBeCorrect)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    Iterator *ic1 = cs1->createIterator();
    ic1->next();
    ic1->next();

    ASSERT_TRUE(ic1->isDone());
    delete cs1, r1, c1;
}
TEST(CaseCompoundIterator, CurrentItemShouldThrowExceptionWhenIsDone)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    Iterator *ic1 = cs1->createIterator();
    ic1->next();
    ic1->next();

    ASSERT_ANY_THROW(ic1->currentItem());
    delete cs1, c1, r1;
}
TEST(CaseCompoundIterator, NextShouldThrowExceptionWhenIsDone)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);
    Iterator *ic1 = cs1->createIterator();
    ic1->next();
    ic1->next();

    ASSERT_ANY_THROW(ic1->next());
    delete cs1, c1, r1;
}