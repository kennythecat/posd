#ifndef COMPOUND_SHAPE_H
#define COMPOUND_SHAPE_H

#include <sstream>
#include "utility.h"
#include "iterator/compound_iterator.h"
#include "circle.h"
#include "rectangle.h"
#include "triangle.h"
#include "visitor/shape_visitor.h"

class CompoundShape : public Shape
{
public:
    virtual ~CompoundShape()
    {
        list<Shape *>::iterator iter;

        for (iter = _shapes->begin(); iter != _shapes->end(); iter++)
        {
            delete *iter;
        }
        _shapes->clear();
    };
    CompoundShape(){};

    double area() const override
    {
        list<Shape *>::iterator iter;
        double _area = 0;

        for (iter = _shapes->begin(); iter != _shapes->end(); iter++)
        {
            _area = _area + (*iter)->area();
        }
        return _area;
    }

    double perimeter() const override
    {
        list<Shape *>::iterator iter;
        double _perimeter = 0;

        for (iter = _shapes->begin(); iter != _shapes->end(); iter++)
        {
            _perimeter = _perimeter + (*iter)->perimeter();
        }
        return _perimeter;
    }

    string info() const override
    {
        stringstream output;
        output << "CompoundShape";
        // list<Shape *>::iterator iter;
        // for (iter = _shapes->begin(); iter != _shapes->end(); iter++)
        // {
        //     output << (*iter)->info() << "";
        // }
        return "CompoundShape";
    }

    Iterator *createIterator() override
    {
        return new CompoundIterator<list<Shape *>::const_iterator>(_shapes->begin(), _shapes->end());
    }

    void addShape(Shape *shape) override
    {
        _shapes->push_back(shape);
    }

    void deleteShape(Shape *shape) override
    {
        list<Shape *>::iterator iter;
        for (iter = _shapes->begin(); iter != _shapes->end(); iter++)
        {
            CompoundShape *cs1;
            // cout << typeid(shape).name() << endl;
            if ((*iter)->info() == "CompoundShape")
            {
                (*iter)->deleteShape(shape);
            }
        }

        _shapes->remove(shape);
    }

    void accept(ShapeVisitor *visitor)
    {
        visitor->visitCompoundShape(this);
    }

private:
    int _count;
    list<Shape *> *_shapes = new list<Shape *>;
};

#endif