// you don't have to modify this class
#ifndef ITERATOR_H
#define ITERATOR_H
#include<iostream>
//#include "../shape.h"

using namespace std;
class Shape;
class Iterator {
public:
    virtual ~Iterator(){}
    
    virtual void first() = 0;

    virtual Shape* currentItem() const = 0;

    virtual void next() = 0;

    virtual bool isDone() const = 0;
    
};
#endif