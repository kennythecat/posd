#include "input_handler.h"

int compound = 0;
bool compounded = false;
bool isempty = true;
ShapeBuilder *builder = ShapeBuilder::getInstance();
Shape *result;
CompoundShape *compound_result;

void InputHandler::handle()
{
    string input = "0";
    printEditorInstructions();
    cin >> input;
    if (input <= "6" && input >= "1")
    {
        handleEditorInstructions(stoi(input));
    }
    else
    {
        cout << "Invalid instruction. Please try again." << endl;
        handle();
    }
}

void InputHandler::printEditorInstructions()
{
    if (compound > 0)
    {
        cout << "Please enter the following instruction number number to start building the compound\n\
        1. 'add circle': to add a circle\n\
        2. 'add rectangle': to add a rectangle\n\
        3. 'add triangle': to add a triangle\n\
        4. 'add compound': to add a compound\n\
        5. 'exit': to exit the program"
             << endl;
    }
    else
    {
        printCompoundInstructions();
    }
}

void InputHandler::handleEditorInstructions(int instruction)
{
    if (compound <= 0)
    {
        switch (instruction)
        {
        case 1:
            addCircle();
            break;
        case 2:
            addRectangle();
            break;
        case 3:
            addTriangle();
            break;
        case 4:
            addCompound();
            break;
        case 5:
            save();
        case 6:
            builder->reset();
            break;
        default:
            cout << "Invalid instruction. Please try again." << endl;
            handle();
        }
    }
    else
    {
        handleCompoundInstructions(instruction);
    }
}

void InputHandler::save()
{
    string output;
    cout << "Please enter the file name to save the shape:" << endl;
    cin >> output;
    string filename = output + ".txt";
    ofstream MyFile(filename);
    if (isempty)
    {
        MyFile.close();
    }
    else
    {
        result = builder->getShape();
        if (compounded)
        {

            ShapeInfoVisitor visitor; //
            result->accept(&visitor); //
            printCompoundInstructions();
            MyFile << visitor.getResult();
        }
        else
        {
            cout << result->info() << endl;
            MyFile << result->info();
        }
    }
    MyFile.close();
    cout << "Save complete." << endl;
    handle();
}

void InputHandler::addCircle()
{
    isempty = false;
    double radius;
    cout << "Please enter the information of circle:\nRadius of circle: ";
    cin >> radius;
    if (radius < 0)
    {
        cout << "Invalid instruction. Please try again." << endl;
        handle();
    }
    builder->buildCircle(radius);
    cout << "Circle added." << endl;
    handle();
}

void InputHandler::addRectangle()
{
    isempty = false;
    double width, length;
    cout << "Please enter the information of rectangle:\nWidth of rectangle: ";
    cin >> width;
    cout << "Height of rectangle: ";
    cin >> length;
    if (width < 0 || length < 0)
    {
        cout << "Invalid instruction. Please try again." << endl;
        handle();
    }

    builder->buildRectangle(width, length);
    result = builder->getShape();
    cout << "Rectangle added." << endl;
    handle();
}

void InputHandler::addTriangle()
{
    isempty = false;
    double x1, y1, x2, y2;
    cout << "Please enter the information of triangle:\nX1 of triangle: ";
    cin >> x1;
    cout << "Y1 of triangle: ";
    cin >> y1;
    cout << "X2 of triangle: ";
    cin >> x2;
    cout << "Y1 of triangle: ";
    cin >> y2;
    if (y1 * x2 - x1 * y2 == 0)
    {
        cout << "Invalid instruction. Please try again." << endl;
        handle();
    }
    builder->buildTriangle(x1, y1, x2, y2);
    cout << "Triangle added." << endl;
    handle();
}

void InputHandler::printCompoundInstructions()
{
    // result = builder->getShape();
    // ShapeInfoVisitor visitor; //
    // result->accept(&visitor); //
    // cout << visitor.getResult() << endl;
    cout << "Please enter the following instruction number number to start building\n\
        1. 'add circle': to add a circle\n\
        2. 'add rectangle': to add a rectangle\n\
        3. 'add triangle': to add a triangle\n\
        4. 'add compound': to add a compound\n\
        5. 'save': output shape to file\n\
        6. 'exit': to exit the program"
         << endl;
}

void InputHandler::handleCompoundInstructions(int instruction)
{
    switch (instruction)
    {
    case 1:
        addCircle();
        break;
    case 2:
        addRectangle();
        break;
    case 3:
        addTriangle();
        break;
    case 4:
        addCompound();
        break;
    case 5:
        compound--;
        builder->buildCompoundEnd();
        handle();
        break;
    }
}

void InputHandler::addCompound()
{
    isempty = false;
    compounded = true;
    builder->buildCompoundBegin();
    compound++;
    handle();
}