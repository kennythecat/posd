#ifndef UTILITY_H
#define UTILITY_H
#include "shape.h"
#include "iterator/compound_iterator.h"

template <class ShapeConstraint>

Shape *selectShape(Shape *shape, ShapeConstraint constraint)
{
    Iterator *iter = shape->createIterator();
    Shape *result = nullptr;
    for (iter->first(); !iter->isDone(); iter->next())
    {
        if (constraint(iter->currentItem()))
        {
            result = iter->currentItem();
            break;
        }
    }
    delete iter;
    return result;
};

#endif