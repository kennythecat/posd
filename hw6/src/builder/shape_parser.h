#pragma once
#include "./scanner.h"
#include "./shape_builder.h"
#include "../shape.h"
#include <string>
#include <iostream>
#include <fstream>
#include <cstring>
#include <sstream>

using namespace std;

class ShapeParser
{
public:
    // ShapeParser(Scanner* scanner, ShapeBuilder* builder): _scanner(scanner), _builder(builder){
    // }
    ShapeParser(std::string filepath) : _filepath(filepath)
    {
        string myText;
        string result;
        stringstream ss;
        ifstream MyReadFile(_filepath);
        while (getline(MyReadFile, myText))
        {
            ss << myText;
        }
        result = ss.str();
        _scanner = new Scanner(result);
        MyReadFile.close();
    }

    ~ShapeParser()
    {
        delete _builder;
        delete _scanner;
    }

    // void parse(std::string input)
    void parse()
    {
        while (!_scanner->isDone())
        {
            std::string token = _scanner->next();
            if (token == "Circle")
            {
                _builder->buildCircle(_scanner->nextDouble());
            }
            if (token == "Rectangle")
            {
                _builder->buildRectangle(_scanner->nextDouble(), _scanner->nextDouble());
            }
            if (token == "Triangle")
            {
                _builder->buildTriangle(_scanner->nextDouble(), _scanner->nextDouble(), _scanner->nextDouble(), _scanner->nextDouble());
            }
            else if (token == "CompoundShape")
            {
                _scanner->next(); // ignore {
                _builder->buildCompoundBegin();
            }
            else if (token == "}")
            {
                _builder->buildCompoundEnd();
            }
        }
    }

    Shape *getShape()
    {
        return _builder->getShape();
    }

private:
    ShapeBuilder *_builder = new ShapeBuilder();
    Scanner *_scanner;
    std::string _filepath;
};
