#pragma once
using namespace std;
#include <string>
#include <vector>

class Scanner
{
public:
    Scanner(std::string input) : _input(input)
    {
        cout << "-->" << _input << "<--" << endl;
    }
    std::string next()
    {
        inDouble = false;
        std::string result = "";
        for (int i = 0; i <= _input.length(); i++)
        {
            if (i + pos > _input.length())
            {
                string error = "next done";
                throw error;
                break;
            }
            for (auto token : tokenList)
            {
                skipWhiteSpace();
                if (_input.compare(pos + i, token.length(), token) == 0)
                {
                    pos = pos + token.length() + i;
                    result = token;
                    return result;
                }
            }
        }
        return result;
    }
    void skipWhiteSpace()
    {
        inDouble = false;
        while (_input[pos] == ' ' || _input[pos] == '\n' || _input[pos] == '\t')
        {
            pos++;
        }
    }

    double nextDouble()
    {
        if (inDouble)
        {
            pos++;
        }

        inDouble = true;
        std::string s = "";
        skipWhiteSpace();
        run = false;
        while (1)
        {
            if ((_input[pos] <= '9' && _input[pos] >= '0') || _input[pos] == '.')
            {
                s = s + _input[pos];
                run = true;
            }
            else if (pos == _input.length())
            {
                std::string error = "double done";
                throw error;
                break;
            }
            else if (run == true)
            {
                pos++;
                break;
            }

            pos++;
        }
        pos--;
        return std::stod(s);
    }

    bool isDone()
    {
        skipWhiteSpace();
        if (pos == _input.length())
        {
            return true;
        }
        else
        {
            return false;
        }
    }

private:
    int count = 0;
    bool run = false;
    bool inDouble = false;
    std::string _input;
    std::string::size_type pos = 0;
    const std::vector<std::string> tokenList = {"Circle", "Rectangle", "Triangle", "CompoundShape", "(", ")", "[", "]", "{", "}", ","};
};