#ifndef TWO_DIMENSIONAL_VECTOR_H
#define TWO_DIMENSIONAL_VECTOR_H
#include <math.h> 
#include <numeric>
#include <stdio.h>

#include "shape.h"


class TwoDimensionalVector {
public:

    TwoDimensionalVector(double x, double y) :  _x(x) , _y(y)
    {

    }

    double x() const 
    { 

        return  _x; 
    }

    double y() const { return _y; }

    double length() const 
    { 
        return sqrt(pow(_x,2)+pow(_y,2));

    }

    double dot(TwoDimensionalVector vec ) const 
    { 
        return _x*vec._x + _y*vec._y;
    }

    double cross(TwoDimensionalVector vec) const 
    {
        return _x*vec._y - _y*vec._x  ;
    }

    TwoDimensionalVector subtract(TwoDimensionalVector vec) const
    { 
        vec._x=_x-vec._x;
        vec._y=_y-vec._y;
     
        return vec;
    }

    std::string info() const 
    {
        double x1 = _x;
        double y1 = _y;
        x1 = (round(x1*100))/100;
        y1 = (round(y1*100))/100;

        std::stringstream ss;
        ss<<"["<<std::fixed<<std::setprecision(2)<<x1<<","<<y1<<"]";
        return ss.str();
    }
    private:
    double _x, _y;   
};
#endif




 
  