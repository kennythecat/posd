# Pattern Oriented Software Design, Fall 2021

## Table of Contents
- [hw1](./hw1) - Abstraction & Error Handling
- [hw2](./hw2) - Implement Composite & Modularity 
- [hw3](./hw3) - Implement Iterator pattern
- [hw4](./hw4) - Implement Visitor pattern
- [hw5](./hw5) - Implement Builder pattern
- [hw6](./hw6) - Singleton pattern & Synchronization 
- [midterm](./midterm) - implement a multipurpose document generator 
- [factory](./factory/) - Implement Factory pattern
***

## Description

This repository contains the following assignments and labs :

- `hw1` (2021/12/06)
  
  Implement prototype, Shape objects, practice to use TDD, and learn how to handle exception.
  
  ![prototype](./png/prototype.png "prototype")

- `hw2` (2021/10/18)
  
  Implement a simple markdown generator. In this system, there are three classes `Paragraph`, `ListItem` and `Text` which are all derived from abstract class `Article`.

  ![composite](./png/composite.png "composite")


- `hw3` (2021/11/04)

  Implement `Iterator` pattern, change data structure of `Shape` container in `CompoundShape` from array to `std::list` and practice to use template in C++.
  
  ![iterator](./png/iterator.png "iterator")

- `hw4` (2021/12/01)
  
  Implement `Visitor` pattern.

  ![visitor](./png/visitor.png "visitor")

- `hw5` (2021/12/15)
  
  Implement Builder pattern and practice how to read text from a file in C++.

  ![builder](./png/builder.png "builder")
  
- `hw6` (2021/12/27)

  Implement singleton pattern on Builder and implement console I/O subsystem.

  ![singleton](./png/singleton.png "singleton")

- `midterm` (2021/11/10)

  Implement a multipurpose document generator for markdown and html. In this system, there are three abstract classes Article, Iterator and ArticleVisitor which are responsible for document, data traversal and generating document representation, respectively.
  
- `factory` (2023/9/13)
  
  Implement Factory pattern

  ![factory](./png/factory.png "factory")

## Rivision history : v1.1
