#ifndef PARAGRAPH_H
#define PARAGRAPH_H
#include"article.h"
class Paragraph : public Article {
   public:
    Paragraph(int level, std::string text) {
        _level = level;
        _text = text + "\n";
        if(_level < 1 || _level >6)throw "level error\n";
    }

    ~Paragraph() {}

    std::string getText() const override {
        int l = getLevel();
        std::string str = _text;
        str = "# " + str;
        for(int i=1;i<l;i++)
            str = "#" + str;
        if(count)
            if(str.rfind("\n") == str.length()-1)
                str.erase(str.length()-1);
        return str;
    }

    int getLevel() const override {
        return _level;
    }

    void add(Article* content) override {
        count++;
        if(content->getLevel() != 0){
            if(content->getLevel() > _level)
                _text += content->getText();
            else
                throw("m not bigger than n!\n");
        }
        else
        {   
            _text += content->getText(); 
            _text += "\n";
        }
    }
private:
    int _level;
    std::string _text;
    int count = 0 ;
};
#endif