#ifndef LISTITEM_H
#define LISTITEM_H
#include "article.h"
class ListItem : public Article {
public:
    ListItem(std::string text) {
        _text = text;
    }

    std::string getText() const {
        return "- " + _text;
    }
    int getLevel() const{
        return level;
    }
    void add(Article* dpFormat){
        throw("not a paragraph!\n");
    }
private:
    std::string _text;
    const int level = 0;
};
#endif