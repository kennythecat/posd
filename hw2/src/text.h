#ifndef TEXT_H
#define TEXT_H
#include "article.h"
class Text : public Article {
public:
    Text(std::string text) {
        _text = text;
    }

    std::string getText() const {
        return _text;
    };
    
    int getLevel() const{
        return level;
    }

    void add(Article* dpFormat){
        throw("not a paragraph!\n");
    }
private:
    std::string _text;
    const int level = 0;
};
#endif