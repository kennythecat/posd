#include"../src/text.h"
TEST(CaseText, get_text){
    Text t("test");
    ASSERT_EQ(t.getText(),"test");
 }

TEST(CaseText, get_level){
    Text t("test");
    ASSERT_EQ(t.getLevel(),0);
 }

 TEST(CaseText, add){
     Text t("test");
     ASSERT_ANY_THROW(t.add(new Text("test2")));
 }