#include "../src/list_item.h"
 
TEST(Caselist_item, get_text){
    ListItem list("test");
    ASSERT_EQ(list.getText(),"- test");
 }

TEST(Caselist_item, get_level){
    ListItem list("test");
    ASSERT_EQ(list.getLevel(),0);
 }

TEST(Caselist_item, add){
     ListItem list("test");
     ASSERT_ANY_THROW(list.add(new ListItem("test2")));
 }