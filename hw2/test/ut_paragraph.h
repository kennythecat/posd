#include "../src/paragraph.h"
#include "../src/list_item.h"
#include "../src/text.h"
TEST(CaseParagraph, get_text){
    Paragraph p(2,"p1");
    ASSERT_EQ(p.getText(),"## p1\n");
 }
 TEST(CaseParagraph, init){
     ASSERT_ANY_THROW(Paragraph p(0,"p1"));
     ASSERT_ANY_THROW(Paragraph p(7,"p1"));
 }

TEST(CaseParagraph, get_level){
    Paragraph p(1,"p1");
    ASSERT_EQ(p.getLevel(),1);
}

TEST(CaseParagraph, add1){
    Paragraph p(1,"p1");
    p.add(new Text("t1"));
    ASSERT_EQ(p.getText(),"# p1\nt1");
 }

TEST(CaseParagraph, add2){
    Paragraph p(1,"p1");
    Paragraph *p2 = new Paragraph(4,"p2");
    p.add(new ListItem("l1"));
    p2->add(new Text("t1"));
    p.add(p2);
    ASSERT_EQ(p.getText(),"# p1\n- l1\n#### p2\nt1");
    ASSERT_ANY_THROW(p2->add(new Paragraph(1,"p1")));
 }