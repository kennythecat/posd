#include "../src/circle.h"
#include "../src/rectangle.h"
#include "../src/utility.h"
#include "../src/compound_shape.h"
TEST(CaseCompoundShape, createCompoundShape){
    Shape *c = new Circle(5.0);
    Shape *r = new Rectangle(5.0,5.0);
    CompoundShape *cs = new CompoundShape();
    cs->addShape(c);
    cs->addShape(r);
    delete c;
    delete r;
    delete cs;
}
TEST(CaseCompoundShape, AddShape){
    CompoundShape* cs = new CompoundShape();
    Circle *c1 = new Circle(5.0);
    cs->addShape(c1);
    ASSERT_EQ(cs->createIterator()->currentItem(),c1);
    delete cs;
    delete c1;
}
TEST(CaseCompoundShape, DeleteShape){
    CompoundShape* cs = new CompoundShape();
    Circle *c1 = new Circle(5.0);
    Rectangle *r1 = new Rectangle(10.0,10.0);
    cs->addShape(c1);
    cs->addShape(r1);
    cs->deleteShape(c1);
    ASSERT_EQ(cs->createIterator()->currentItem(),r1);
    delete cs;
    delete c1;
    delete r1;
}
TEST(CaseCompoundShape, Area){
    Shape *r1 = new Rectangle(5.0,5.0);
    Shape *r2 = new Rectangle(10.0,10.0);
    CompoundShape *cs = new CompoundShape();
    cs->addShape(r1);
    cs->addShape(r2);
    double result = cs->area();
    ASSERT_EQ(result,125.0);
    delete r1;
    delete r2;
    delete cs;
}
TEST(CaseCompoundShape, Perimeter){
    Shape *r1 = new Rectangle(5.0,5.0);
    Shape *r2 = new Rectangle(10.0,10.0);
    CompoundShape *cs = new CompoundShape();
    cs->addShape(r1);
    cs->addShape(r2);
    double result = cs->perimeter();
    ASSERT_EQ(result,60.0);
    delete r1;
    delete r2;
    delete cs;
}
TEST(CaseCompoundShape, Info){
    CompoundShape* cs1 = new CompoundShape();
    cs1->addShape(new Circle(1.1));
    cs1->addShape(new Rectangle(3.14 ,4));

    CompoundShape* cs2 = new CompoundShape();
    cs2->addShape(new Circle(12.34567));
    cs2->addShape(cs1);

    ASSERT_EQ(cs2->info(), "CompoundShape\n{\nCircle (12.35)\nCompoundShape\n{\nCircle (1.10)\nRectangle (3.14 4.00)\n}\n}");
    delete cs1;
    delete cs2;
}