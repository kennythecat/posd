#include"../src/rectangle.h"
#include<string>
TEST(CaseRectangle,Constructor){
    ASSERT_ANY_THROW(new Rectangle());
}
TEST(CaseRectangle,PositiveLengthCheck){
    ASSERT_ANY_THROW(new Rectangle(-10.0,5.0));
    ASSERT_ANY_THROW(new Rectangle(0.0,5.0));
}
TEST(CaseRectangle,PositiveWidthCheck){
    ASSERT_ANY_THROW(new Rectangle(10.0,-5.0));
    ASSERT_ANY_THROW(new Rectangle(5.0,0.0));
}

TEST(CaseRectangle,Area){
    Rectangle r(10.0,5.0);
    ASSERT_NEAR(50.0,r.area(),0.01);
}
TEST(CaseRectangle,Perimeter){
    Rectangle r(10.0,5.0);
    ASSERT_NEAR(30.0,r.perimeter(),0.01);
}
TEST(CaseRectangle,Info){
    Rectangle r(10.0,5.0);
    EXPECT_EQ(r.info().compare("Rectangle (10.00 5.00)"),0);
}
TEST(CaseRectangle,addShape){
    Rectangle r(10.0,5.0);
    ASSERT_ANY_THROW(r.addShape(new Rectangle(5.0)));
}
TEST(CaseRectangle,deleteShape){
    Rectangle r(10.0,5.0);
    ASSERT_ANY_THROW(r.deleteShape(new Rectangle(5.0)));
}
TEST(CaseRectangle,IsDone){
    Rectangle r(10.0,5.0);
    ASSERT_TRUE(r.createIterator()->isDone());
}
TEST(CaseRectangle,ShouldBeAShape){
    Rectangle r(10.0,5.0);
    ASSERT_TRUE(typeid(r) == typeid(Rectangle));
}