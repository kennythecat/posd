#include "../src/two_dimensional_vector.h"
TEST(CaseVector,Constructor){
    ASSERT_ANY_THROW(new TwoDimensionalVector(10.0));
    ASSERT_ANY_THROW(new TwoDimensionalVector());
}
TEST(CaseVector,CheckPoint){
    TwoDimensionalVector vec(10.0,10.0);
    ASSERT_EQ(10.0, vec.x());
    ASSERT_EQ(10.0, vec.y());
}
TEST(CaseVector,CheckLength){
    TwoDimensionalVector vec(10.0,10.0);
    ASSERT_NEAR(sqrt(2*10.0*10.0), vec.length(),0.01);
}

TEST(CaseVector,Subtract){
    TwoDimensionalVector vec(10.0,10.0);
    TwoDimensionalVector vec2(5.0,5.0);
    //TwoDimensionalVector vec_temp = vec.subtract(vec2);
    ASSERT_EQ(vec.subtract(vec2).x(),5.0);
    ASSERT_EQ(vec.subtract(vec2).y(),5.0);
}

TEST(CaseVector,DotProduct){
    TwoDimensionalVector vec(10.0,10.0);
    TwoDimensionalVector vec2(5.0,5.0);
    ASSERT_EQ(vec.dot(vec2),100.0);
}

TEST(CaseVector,CrossProduct){
    TwoDimensionalVector vec(10.0,10.0);
    TwoDimensionalVector vec2(5.0,5.0);
    ASSERT_EQ(vec.cross(vec2),0);
}

TEST(CaseVector,Info){
    TwoDimensionalVector vec(10.0,-2.30);
    EXPECT_EQ(vec.info().compare("[10.00,-2.30]"),0);
}
