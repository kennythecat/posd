#include"../src/utility.h"
TEST(CaseUtility, SelectShapeByArea) {
    Shape* c1 = new Circle(1.0);
    Shape* c2 = new Circle(1.0);
    Shape* r = new Rectangle(5.0,5.0);
    CompoundShape *cs = new CompoundShape();
    cs->addShape(c1);
    cs->addShape(c2);
    cs->addShape(r);
    
    Shape* result = selectShape(cs, [](Shape* shape){
        return shape->area() > 20.0 && shape->area() < 30.0;
    });

    ASSERT_EQ(r, result);
    delete c1;
    delete c2;
    delete r;
    delete cs;
}
TEST(CaseUtility, SelectShapeByType) {
    Shape* c1 = new Circle(1.0);
    Shape* c2 = new Circle(1.0);
    Shape* r = new Rectangle(5.0,5.0);
    CompoundShape *cs = new CompoundShape();
    cs->addShape(c1);
    cs->addShape(c2);
    cs->addShape(r);
    
    // Shape* result = selectShape(cs, [](Shape* shape){
    //     return typeid(shape) == typeid(r);
    // });

    // ASSERT_EQ(r, result);
    delete c1;
    delete c2;
    delete r;
    delete cs;
}
