#include"../src/triangle.h"
TEST(CaseTriangle,Constructor){
    TwoDimensionalVector vec1(4.0,4.0);
    ASSERT_ANY_THROW(new Triangle(vec1));
    ASSERT_ANY_THROW(new Triangle());
}
TEST(CaseTriangle,Length){
    TwoDimensionalVector vec1(4.0,4.0),vec2(4.0,4.0);
    TwoDimensionalVector vec3(4.0,4.0),vec4(5.0,5.0);
    ASSERT_ANY_THROW(new Triangle(vec1,vec2));
    ASSERT_ANY_THROW(new Triangle(vec3,vec4));
}
TEST(CaseTriangle,Area){
    TwoDimensionalVector vec1(4.0,4.0),vec2(3.0,2.0);
    Triangle T(vec1,vec2);
    ASSERT_NEAR(0.5 * abs(4*2-3*4),T.area(),0.01);
}
TEST(CaseTriangle,Perimeter){
    TwoDimensionalVector vec1(4.0,4.0),vec2(3.0,2.0);
    Triangle T(vec1,vec2);
    ASSERT_NEAR(sqrt(4*4+4*4) + sqrt(3*3+2*2) + sqrt(1*1+2*2) , T.perimeter() , 0.01);
}
TEST(CaseTriangle,Info){
    TwoDimensionalVector vec1(3.0,12.43), vec2(17.57,-4.00);
    Triangle T(vec1,vec2);
    EXPECT_EQ(T.info().compare("Triangle ([3.00,12.43] [17.57,-4.00])"),0);
}
TEST(CaseTriangle,addShape){
    TwoDimensionalVector vec1(3.0,12.43), vec2(17.57,-4.00);
    Triangle T(vec1,vec2);
    ASSERT_ANY_THROW(T.addShape(new Triangle(vec1,vec2)));
}
TEST(CaseTriangle,deleteShape){
    TwoDimensionalVector vec1(3.0,12.43), vec2(17.57,-4.00);
    Triangle T(vec1,vec2);
    ASSERT_ANY_THROW(T.deleteShape(new Triangle(vec1,vec2)));
}
TEST(CaseTriangle,IsDone){
    TwoDimensionalVector vec1(3.0,12.43), vec2(17.57,-4.00);
    Triangle T(vec1,vec2);
    ASSERT_TRUE(T.createIterator()->isDone());
}
TEST(CaseTriangle,ShouldBeAShape){
    TwoDimensionalVector vec1(3.0,12.43), vec2(17.57,-4.00);
    Triangle T(vec1,vec2);
    ASSERT_TRUE(typeid(T) == typeid(Triangle));
}