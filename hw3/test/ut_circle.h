#include "../src/circle.h"
#include<string>
TEST(CaseCircle,Constructor){
    ASSERT_ANY_THROW(new Circle());
}
TEST(CaseCircle,PositiveCheck){
    ASSERT_ANY_THROW(new Circle(0.0));
    ASSERT_ANY_THROW(new Circle(-1.0));
}

TEST(CaseCircle,Area){
    Circle c(10.0);
    ASSERT_NEAR(314.1592, c.area(), 0.01);
}
TEST(CaseCircle,Perimeter){
    Circle c(10.0);
    ASSERT_NEAR(20.0*3.141592,c.perimeter(),0.01);
}
TEST(CaseCircle,Info){
    Circle c(10.0);
    EXPECT_EQ(c.info().compare("Circle (10.00)"),0);
}
TEST(CaseCircle,addShape){
    Circle c(10.0);
    ASSERT_ANY_THROW(c.addShape(new Circle(5.0)));
}
TEST(CaseCircle,deleteShape){
    Circle c(10.0);
    ASSERT_ANY_THROW(c.deleteShape(new Circle(5.0)));
}
TEST(CaseCircle,IsDone){
    Circle c(10.0);
    ASSERT_TRUE(c.createIterator()->isDone());
}
TEST(CaseCircle,ShouldBeAShape){
    Circle c(10.0);
    ASSERT_TRUE(typeid(c) == typeid(Circle));
}