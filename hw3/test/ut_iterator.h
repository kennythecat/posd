#include "../src/shape.h"
#include "../src/circle.h"
#include "../src/iterator/null_iterator.h"
TEST(CaseNullIterator, NullIteratorIsAIterator){
    Iterator* it = new NullIterator();
    ASSERT_TRUE(typeid(it) == typeid(Iterator*));
    delete it;
}
TEST(CaseNullIterator, NullIterator_isDone){
    Iterator* it = new NullIterator();
    ASSERT_TRUE(it->isDone());
    delete it;
}
TEST(CaseNullIterator, NullIterator_first){
    Iterator* it = new NullIterator();
    ASSERT_ANY_THROW(it->first());
    delete it;
}
TEST(CaseNullIterator, NullIterator_currentItem){
    Iterator *it = new NullIterator();
    ASSERT_ANY_THROW(it->currentItem());
    delete it;
}
TEST(CaseNullIterator, NullIterator_next){
    Iterator *it = new NullIterator();
    ASSERT_ANY_THROW(it->next());
    delete it;
}
TEST(CaseCompoundIterator, CompoundIteratorIsAIterator){
    std::list<Shape*> _shapes;
    Iterator* it = new CompoundIterator<std::list<Shape*>::iterator>(_shapes.begin(),_shapes.end() );
    ASSERT_TRUE(typeid(it) == typeid(Iterator*));
    delete it;
}
TEST(CaseCompoundIterator, FirstNoException){
    CompoundShape *cs = new CompoundShape();
    Shape * c = new Circle(5.0);
    cs->addShape(c);
    Iterator * it = cs->createIterator();
    it->first();
    ASSERT_EQ(it->currentItem(), c);
    delete cs;
    delete c;
    delete it;
}
TEST(CaseCompoundIterator, CurrentItemIsCorrect){
    CompoundShape *cs = new CompoundShape();
    Shape * c = new Circle(5.0);
    cs->addShape(c);
    Iterator * it = cs->createIterator();
    ASSERT_EQ(it->currentItem(), c);
    delete cs;
    delete c;
    delete it;
}
TEST(CaseCompoundIterator, NextNoException){
    CompoundShape *cs = new CompoundShape();
    Shape * c1 = new Circle(5.0);
    Shape * c2 = new Circle(10.0);
    cs->addShape(c1);
    cs->addShape(c2);
    Iterator * it = cs->createIterator();
    it->next();
    ASSERT_EQ(it->currentItem(), c2);
    delete cs;
    delete c1;
    delete c2;
    delete it;
}
TEST(CaseCompoundIterator, IsDoneShouldBeCorrect){
    CompoundShape *cs = new CompoundShape();
    Shape * c = new Circle(5.0);
    cs->addShape(c);
    Iterator * it = cs->createIterator();
    it->next();
    ASSERT_TRUE(it->isDone());
    delete cs;
    delete c;
    delete it;
}
TEST(CaseCompoundIterator, CurrentItemShouldThrowExceptionWhenIsDone){
        CompoundShape *cs = new CompoundShape();
    Shape * c = new Circle(5.0);
    cs->addShape(c);
    Iterator * it = cs->createIterator();
    it->next();
    ASSERT_ANY_THROW(it->currentItem());
    delete cs;
    delete c;
    delete it;
}
TEST(CaseCompoundIterator, NextShouldThrowExceptionWhenIsDone){
        CompoundShape *cs = new CompoundShape();
    Shape * c = new Circle(5.0);
    cs->addShape(c);
    Iterator * it = cs->createIterator();
    it->next();
    ASSERT_ANY_THROW(it->next());
    delete cs;
    delete c;
    delete it;
}