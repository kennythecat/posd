#ifndef CIRCLE_H
#define CIRCLE_H
#include<cmath>
#include<string>
#include<sstream>
#include"iterator/iterator.h"
#include"iterator/null_iterator.h"
class Circle : public Shape {
public:
    Circle(){
        throw "no default radius!";
    }
    Circle(double radius) {
        if( radius <= 0 )
            throw "radius <= 0 !";
        _radius = radius;
    }

    double area() const override { 
        return _radius * _radius * M_PI;
    }

    double perimeter() const override { 
        return 2 * _radius * M_PI;
    }

    std::string info() const override { 
        std::stringstream s1;
        double x = _radius;
        x *= 100;
        int y = round(x); x = y;
        int count = 0, check = 0;
        if(y % 10 == 0){count++; check++;}
        y /= 10; x /= 10;
        if(count == 1 && y % 10 == 0){count++; check--;}
        y /= 10; x /= 10;
        s1 << x;
        std::string s = s1.str();
        if(count && !check){s += ".0"; count--;}
        if(count)s += "0";
        return _info + s + ")";
    }
    void addShape(Shape* shape)override { 
        throw("can not add!");
    }
    void deleteShape(Shape* shape)override{
        throw("can not delete");
    }

    Iterator* createIterator() override { 
        return new NullIterator(); 
    }
private:
    double _radius;
    std::string _info = "Circle (";
};

#endif