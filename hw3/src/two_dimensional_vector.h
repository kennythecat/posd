#ifndef TWO_DIMENSIONAL_VECTOR_H
#define TWO_DIMENSIONAL_VECTOR_H
class TwoDimensionalVector {
public:
    TwoDimensionalVector(){
        throw("parameter error");
    }
    TwoDimensionalVector(double x){
        throw("parameter error");
    }
    TwoDimensionalVector(double x, double y) : _x(x),_y(y){}

    double x() const {  return _x;}

    double y() const { return _y;}

    double length() const { return sqrt(_x * _x + _y * _y);}

    double dot(TwoDimensionalVector vec) const { 
        double a = vec.x() * this->x();
        double b = vec.y() * this->y();
        return a + b;
    }

    double cross(TwoDimensionalVector vec) const {
        double a = this->x() * vec.y();
        double b = vec.x() * this->y();
        return a - b; 
     }

    TwoDimensionalVector subtract(TwoDimensionalVector vec) const { 
        double a = this->x() - vec.x();
        double b = this->y() - vec.y();
        TwoDimensionalVector vec_temp(a,b);
        return vec_temp;
    }

    std::string info() const { 
        double num[2]={_x,_y};
        std::string s[2]={};
        for(int i=0;i<2;i++){
            std::stringstream ss;
            num[i]*=100;
            int y = round(num[i]);
            num[i] = y;
            int count = 0, check = 0;
            if(y%10 == 0){count++;check++;}
            y/=10;num[i]/=10;
            if(count==1 && y%10==0){count++;check--;}
            y/=10;num[i]/=10;
            ss<<num[i];
            s[i]=ss.str();
            if(count&&!check){s[i]+=".0";count--;}
            if(count)s[i]+="0";
        }
        return _info + s[0] + "," + s[1] + "]";
    }
private:
    double _x;
    double _y;
    std::string _info = "[";
};
#endif