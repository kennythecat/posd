#ifndef TRIANGLE_H
#define TRIANGLE_H
#include<string>
#include<cmath>
#include<sstream>
#include"two_dimensional_vector.h"
#include"shape.h"
#include"iterator/iterator.h"
#include"iterator/null_iterator.h"
class Triangle: public Shape {
public:
    Triangle(){
        throw("parameter error!");
    }
    Triangle(TwoDimensionalVector vec1){
        throw("parameter error!");
    }
    Triangle(TwoDimensionalVector vec1, TwoDimensionalVector vec2):_vec1(vec1),_vec2(vec2) { 
        if(_vec1.cross(_vec2)==0)
            throw ("two vector are parellel");
    }

    double area() const override { 
        return 0.5 * abs(_vec1.x()*_vec2.y()-_vec2.x()*_vec1.y());
    }

    double perimeter() const override { 
        double a = sqrt((_vec1.x() - _vec2.x()) * (_vec1.x() - _vec2.x()) + ((_vec1.y() - _vec2.y()) * (_vec1.y() - _vec2.y())));
        double b = sqrt(_vec1.x() * _vec1.x() + _vec1.y() * _vec1.y());
        double c = sqrt(_vec2.x() * _vec2.x() + _vec2.y() * _vec2.y());
        return a + b + c;
    }

    std::string info() const override { 
        double num[4]={_vec1.x(),_vec1.y(),_vec2.x(),_vec2.y()};
        std::string s[4]={};
        for(int i = 0;i < 4;i++){
            std::stringstream ss;
            num[i] *= 100;
            int y = round(num[i]);
            num[i] = y;
            int count = 0, check = 0;
            if(y%10 == 0){count++;check++;}
            y/=10;num[i]/=10;
            if(count == 1 && y % 10 == 0){count++;check--;}
            y/=10;num[i]/=10;
            ss<<num[i];
            s[i]=ss.str();
            if(count && !check){s[i]+=".0";count--;}
            if(count)s[i]+="0";
        }
        return _info + s[0] + "," + s[1] +\
            "] [" + s[2] + "," + s[3] + "])";
    }

    void addShape(Shape* shape)override{ 
        throw("can not add!");
    }
    void deleteShape(Shape* shape)override{
        throw("can not delete");
    }

    Iterator* createIterator() override { 
        return new NullIterator(); 
    }
private:
    TwoDimensionalVector _vec1;
    TwoDimensionalVector _vec2;
    std::string _info = "Triangle ([" ;
};
#endif