#ifndef COMPOUND_SHAPE_H
#define COMPOUND_SHAPE_H
#include<string>
#include<list>
#include "iterator/iterator.h"
#include "iterator/compound_iterator.h"
#include "shape.h"
class CompoundShape : public Shape {
public:
    ~CompoundShape() { }
    double area() const override { 
        double result = 0;
        for(Shape *s : _shapes)
            result += s->area();
        return result;
    }

    double perimeter() const override { 
        double result = 0;
        for(Shape *s : _shapes)
            result += s->perimeter();
        return result;
    }

    std::string info() const override {
        std::string str="CompoundShape\n{\n";
        for(Shape *s : _shapes){
            str += s->info();
            str += "\n";
        }
        str += "}";
        return str;
     }

    Iterator* createIterator() override { 
        return new CompoundIterator<std::list<Shape*>::iterator>(_shapes.begin(),_shapes.end());
    }

    void addShape(Shape* shape) override { 
        _shapes.push_back(shape);
    }

    void deleteShape(Shape* shape) override { 
        _shapes.remove(shape);
    } 
private:
    std::list<Shape*> _shapes; //= new std::list<Shape*>;
};
#endif