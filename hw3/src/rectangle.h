#ifndef RECTANGLE_H
#define RECTANGLE_H
#include<cmath>
#include<string.h>
#include<sstream>
#include"shape.h"
#include"iterator/iterator.h"
#include"iterator/null_iterator.h"
class Rectangle : public Shape {
public:
    Rectangle(){
        throw("parameter error!");
    }
    Rectangle(double x){
        throw("one parameter is missing!");
    }
    Rectangle(double length, double width) { 
        if( length <= 0 ) 
            throw "length <= 0 !";
        if( width <= 0 ) 
            throw "width <= 0 !";
        _length = length;
        _width = width;
    }

    double length() const{
        return _length;
    }
    double width() const{
        return _width;
    }
    double area() const override { 
        return _length*_width;
    }

    double perimeter() const override { 
        return 2*(_length + _width);
    }
    
    std::string info() const override { 
        double num[2]={_length,_width};
        std::string s[2]={};
        for(int i=0;i<2;i++){
            std::stringstream ss;
            num[i] *= 100;
            int y = round(num[i]);
            num[i] = y;
            int count = 0, check = 0;
            if(y%10 == 0){count++;check++;}
            y/=10;num[i]/=10;
            if(count==1 && y%10==0){count++;check--;}
            y/=10;num[i]/=10;
            ss<<num[i];
            s[i]=ss.str();
            if(count&&!check){s[i]+=".0";count--;}
            if(count)s[i]+="0";
        }
        return _info + s[0] + " " + s[1] + ")";
    }

    void addShape(Shape* shape)override { 
        throw("can not add!");
    }
    void deleteShape(Shape* shape)override{
        throw("can not delete");
    }

    Iterator* createIterator() override { 
        return new NullIterator(); 
    }
private:
    double _length;
    double _width;
    std::string _info = "Rectangle (" ;
};
#endif