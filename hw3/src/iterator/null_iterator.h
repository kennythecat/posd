#ifndef NULL_ITERATOR_H
#define NULL_ITERATOR_H
#include"iterator.h"
class NullIterator : public Iterator {
public:
    void first() override { 
        throw("first error");
    }

    Shape* currentItem() const override { 
        throw("currentItem error");
    }

    void next() override { 
        throw("next error");
    }

    bool isDone() const override {
        return true;
    }
};
#endif