// you should define a template class or type `ForwardIterator`
#ifndef COMPOUND_ITERATOR_H
#define COMPOUND_ITERATOR_H
template<class ForwardIterator>
class CompoundIterator : public Iterator{
public:
    CompoundIterator(ForwardIterator begin, ForwardIterator end): _begin(begin), _end(end){
        first();
    }

    void first() override {
        _current = _begin;
     }

    Shape* currentItem() const override {
        if(isDone())
            throw("end!");
        else
            return *_current;
     }

    void next() override {
        if(isDone())
            throw("end!");
        else 
            _current++;
    }

    bool isDone() const override { 
        return _current == _end;
    }
private:
    ForwardIterator _current;
    ForwardIterator _begin;
    ForwardIterator _end;
};
#endif