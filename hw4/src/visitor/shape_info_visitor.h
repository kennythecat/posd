#pragma once
#include <string>
#include <sstream>
#include <iostream>

#include "./shape_visitor.h"
#include "../iterator/compound_iterator.h"
#include "../iterator/null_iterator.h"
using namespace std;

class ShapeInfoVisitor : public ShapeVisitor
{
public:
        void visitCircle(Circle *c) override
    {
        _shape_level++;
        for (int i = 1; i < _shape_level; i++)
        {
            s << "  ";
        }
        s << c->info() << "\n";
        _shape_level--;
    }
    void visitRectangle(Rectangle *r) override
    {
        _shape_level++;
        for (int i = 1; i < _shape_level; i++)
        {
            s << "  ";
        }
        s << r->info() << "\n";
        _shape_level--;
    }
    void visitTriangle(Triangle *t) override
    {
        _shape_level++;
        for (int i = 1; i < _shape_level; i++)
        {
            s << "  ";
        }
        s << t->info() << "\n";
        _shape_level--;
    }
    void visitCompoundShape(CompoundShape *cs) override
    {
        for (int i = 0; i < _shape_level; i++)
        {
            s << "  ";
        }
        s << cs->info() << "{\n";

        _shape_level++;
        Iterator *it = cs->createIterator();

        for (it->first(); !it->isDone(); it->next())
        {
            it->currentItem()->accept(this);
        }
        for (int i = 1; i < _shape_level; i++)
        {
            s << " ";
        }
    }

    std::string getResult()
    {
        for (int i = 0; i < _shape_level; i++)
        {
            s << "}\n";
        }
        return s.str();
    }

private:
    stringstream s;
    int _shape_level = 0;
    string ss = "he";
};
