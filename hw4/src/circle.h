#ifndef CIRCLE_H
#define CIRCLE_H
#include <cmath>
#include "visitor/shape_visitor.h"
#include "iterator/null_iterator.h"
#include "shape.h"

class Circle : public Shape
{
public:
    // ~Circle()
    // {
    //     delete n1;
    // };
    Circle(double radius) : _radius(radius)
    {
        _radius = radius;
        if (_radius <= 0)
        {
            string error = "Radius should be positive.";
            throw error;
        };
    }

    double area() const override
    {
        return pow(_radius, 2) * M_PI;
    }

    double perimeter() const override
    {
        return _radius * 2 * M_PI;
    }

    string info() const override
    {
        stringstream ss;
        double radius = round(_radius * 100);
        radius = radius / 100;
        ss << "Circle (" << fixed << setprecision(2) << radius << ")";

        return ss.str();
    }

    Iterator *createIterator() override
    {

        return new NullIterator;
    }

    void accept(ShapeVisitor *visitor)
    {
        visitor->visitCircle(this);
    }

private:
    // NullIterator *n1 = new NullIterator();
    double _radius;

protected:
};

#endif