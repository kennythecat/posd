// you should define a template class or type `ForwardIterator`
#ifndef COMPOUND_ITERATOR_H
#define COMPOUND_ITERATOR_H

#include <list>

#include "iterator.h"

//#include "../shape.h"//

template <class ForwardIterator>

class CompoundIterator : public Iterator
{

public:
    CompoundIterator(ForwardIterator begin, ForwardIterator end)
    {
        _begin = begin;
        _end = end;
        _current = begin;
    }

    void first() override
    {
        _current = _begin;
    }

    Shape *currentItem() const override
    {
        if (isDone())
        {
            throw string("Done");
        }
        else
        {
            return *_current;
        }
    }

    void next() override
    {
        if (isDone() == true)
        {
            throw string("Done");
        }
        else
        {
            _current++;
        }
    }

    bool isDone() const override
    {
        if (_current == _end)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

private:
    ForwardIterator _begin;
    ForwardIterator _end;
    ForwardIterator _current;
};

#endif
