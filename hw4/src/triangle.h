#ifndef TRIANGLE_H
#define TRIANGLE_H
#include <math.h>
#include <numeric>
#include "two_dimensional_vector.h"
#include "./iterator/iterator.h"
#include "./iterator/null_iterator.h"
#include "./compound_shape.h"

class Triangle : public Shape
{
public:
  // ~Triangle()
  // {
  //   delete n1;
  // }
  Triangle(TwoDimensionalVector vec1, TwoDimensionalVector vec2) : _vec1(vec1), _vec2(vec2)
  {
    _vec1 = vec1;
    _vec2 = vec2;
    double result = _vec1.y() * _vec2.x() - _vec1.x() * _vec2.y();

    if (((_vec1.x() == 0) && (_vec1.y() == 0)) || ((_vec2.x() == 0) && (_vec2.y() == 0)))
    {
      std::string error = "Both vectors should be greater than zero.";
      throw error;
    }

    if (result == 0)
    {
      std::string error = "Two vectors should not be parallel.";
      throw error;
    }
  }

  double area() const override
  {
    return abs(_vec1.y() * _vec2.x() - _vec1.x() * _vec2.y()) / 2;
  }

  double perimeter() const override
  {
    TwoDimensionalVector _vec3 = _vec1.subtract(_vec2);
    return sqrt(pow((_vec1.x()), 2) + pow((_vec1.y()), 2)) + sqrt(pow((_vec2.x()), 2) + pow((_vec2.y()), 2)) + sqrt(pow((_vec3.x()), 2) + pow((_vec3.y()), 2));
  }

  std::string info() const override
  {
    double x1 = _vec1.x();
    double x2 = _vec2.x();

    double y1 = _vec1.y();
    double y2 = _vec2.y();

    std::stringstream ss;
    ss << "Triangle ([" << std::fixed << std::setprecision(2) << x1 << "," << y1 << "] [" << x2 << "," << y2 << "])";

    return ss.str();
  }

  Iterator *createIterator() override
  {
    return new NullIterator;
  }

  void accept(ShapeVisitor *visitor)
  {
    visitor->visitTriangle(this);
  }

private:
  // NullIterator *n1 = new NullIterator();
  TwoDimensionalVector _vec1, _vec2;
};

#endif