#include <gtest/gtest.h>
#include "../../src/circle.h"
#include "../../src/rectangle.h"
#include "../../src/triangle.h"
#include "../../src/two_dimensional_vector.h"
TEST(CaseNullIterator, NullIteratorIsAIterator)
{
    Circle c1 = Circle(5);
    ASSERT_NO_THROW(Iterator *ic1 = c1.createIterator());
}
TEST(CaseNullIterator, FirstShouldThrowException)
{
    try
    {
        Circle c1 = Circle(5);
        Iterator *ic1 = c1.createIterator();
        ic1->first();
        FAIL();
    }
    catch (string s)
    {
        ASSERT_EQ("Error.", s);
    }
}
TEST(CaseNullIterator, NextShouldThrowException)
{
    try
    {
        Circle c1 = Circle(5);
        Iterator *ic1 = c1.createIterator();
        ic1->next();
        FAIL();
    }
    catch (string s)
    {
        ASSERT_EQ("Error.", s);
    }
}
TEST(CaseNullIterator, CurrentItemShouldThrowException)
{
    try
    {
        Circle c1 = Circle(5);
        Iterator *ic1 = c1.createIterator();
        ic1->currentItem();
        FAIL();
    }
    catch (string s)
    {
        ASSERT_EQ("Error.", s);
    }
}
TEST(CaseNullIterator, IsDoneShouldBeTrue)
{
    Circle c1 = Circle(5);
    Iterator *ic1 = c1.createIterator();
    bool boo = ic1->isDone();
    ASSERT_EQ(true, boo);
}