#include <gtest/gtest.h>
#include "../src/triangle.h"
#include "../src/circle.h"
#include "../src/rectangle.h"
#include "../src/compound_shape.h"
#include "../src/utility.h"

TEST(CaseUtility, SelectShapeByAREA)
{
    CompoundShape *cs = new CompoundShape();
    Circle *c1 = new Circle(1.0);
    Circle *c2 = new Circle(1.0);
    Rectangle *r1 = new Rectangle(5.0, 5.0);

    cs->addShape(c1);
    cs->addShape(c2);
    cs->addShape(r1);

    Shape *result = selectShape(cs, [](Shape *shape)
                                { return shape->area() > 20.0 && shape->area() < 30.0; });

    ASSERT_EQ("Rectangle (5.00 5.00)", result->info());
    delete cs, c1, c2, r1;
}