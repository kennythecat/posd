#include <iostream>
#include "../../src/compound_shape.h"
#include "../../src/visitor/shape_info_visitor.h"

TEST(CaseInfoVIsitor, Creation)
{
    CompoundShape *cs1 = new CompoundShape();
    cs1->addShape(new Circle(1.1));
    cs1->addShape(new Rectangle(3.14, 4));

    CompoundShape *cs2 = new CompoundShape();
    cs2->addShape(new Circle(12.34567));
    cs2->addShape(cs1);

    ShapeInfoVisitor visitor;
    cs2->accept(&visitor);
    // cout << typeid(cs1).name();
    // cout << visitor.getResult();

    ASSERT_EQ("CompoundShape{\n  Circle (12.35)\n  CompoundShape{\n    Circle (1.10)\n    Rectangle (3.14 4.00)\n  }\n}\n", visitor.getResult());
}

// TEST(CaseVisitor, SelectShape)
// {
//     Shape *c2 = new Circle(1.0);
//     Shape *r = new Rectangle(2, 3);
//     CompoundShape *cs1 = new CompoundShape();
//     cs1->addShape(c2);
//     cs1->addShape(r);

//     SelectShapeVisitor *visitor = new SelectShapeVisitor([](Shape *shape)
//                                                          { return shape->area() > 5 && shape->area() < 10; });
//     cs1->accept(visitor);
//     Shape *result = visitor->getShape();

//     ASSERT_EQ(r, result);

//     delete visitor;
// }

// TEST(CaseVisitor, SelectShapeOnCircleNotFound)
// {
//     Circle *c1 = new Circle(1.0);

//     SelectShapeVisitor *visitor = new SelectShapeVisitor([](Shape *shape)
//                                                          { return shape->area() > 20.0 && shape->area() < 30.0; });
//     // c1->accept(visitor);
//     visitor->visitCircle(c1);
//     Shape *result = visitor->getShape();

//     ASSERT_EQ(nullptr, result);
//     delete c1;
//     delete visitor;
// }

// TEST(CaseVisitor, SelectShapeOnCircleThroughVisitor)
// {
//     Circle *c1 = new Circle(3.0);

//     SelectShapeVisitor *visitor = new SelectShapeVisitor([](Shape *shape)
//                                                          { return shape->area() > 20.0 && shape->area() < 30.0; });
//     c1->accept(visitor);
//     Shape *result = visitor->getShape();

//     ASSERT_EQ(c1, result);

//     delete c1;
//     delete visitor;
// }

// TEST(CaseVisitor, SelectShapeOnCircle)
// {
//     Circle *c2 = new Circle(3.0);

//     SelectShapeVisitor *visitor = new SelectShapeVisitor([](Shape *shape)
//                                                          { return shape->area() > 20.0 && shape->area() < 30.0; });

//     visitor->visitCircle(c2);
//     Shape *result = visitor->getShape();

//     ASSERT_EQ(c2, result);

//     delete c2;
//     delete visitor;
// }