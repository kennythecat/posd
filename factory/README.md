# Factory Pattern - Side project

## Table of Contents
- [Factory Pattern - Side project](#factory-pattern---side-project)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [Applicability](#applicability)
  - [Participants](#participants)
  - [Related Patterns](#related-patterns)
  - [File Structure](#file-structure)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description
Implement factory pattern based on [HW1](../hw1/Assignment1.md)
Provide an interface for creating families of related or dependent objects without
specifying their concrete classes.

## Applicability
Use the Abstract Factorypattern when
- a system should be independent of how its products are created, composed,
and represented.
- a system should be configured with one of multiple families of products.
- a family of related product objects is designed to be used together, and you
need to enforce this constraint.
- you want to provide a class library of products, and you want to reveal just
their interfaces, not their implementations

## Participants
- AbstractFactory ([Factory](./src/shape_factory.h))
  - declares an interface for operations that create abstract product objects.
- ConcreteFactory ([Factory](./src/shape_factory.h))
  - implements the operations to create concrete product objects.
- AbstractProduct ([Shape](./src/shape.h))
  - declares an interfacefor a type of product object.
- ConcreteProduct ([Circle](./src/circle.h), [Rectangle](./src/rectangle.h))
  - defines a product objecttobecreated bythe corresponding concrete factory.
  - implements theAbstractProduct interface.
- Client([UnitTest](./test/ut_shape_factory.h))
  - uses only interfaces declared by AbstractFactory and AbstractProduct
classes

## Related Patterns
AbstractFactory classes are often implemented with factory methods (Factory
Method), but they can also be implemented using [Prototype](../hw1/Assignment1.md).
A concrete factory is often a [Singleton](../hw6/Assignment6.md).

## File Structure
```
|-- Makefile
|-- bin
|-- src
|   |-- circle.h
|   |-- shape.h
|   |-- rectangle.h
|   |-- triangle.h
|   |-- two_dimensional_vector.h
|-- test
|   |-- ut_main.cpp
|   |-- ut_circle.h
|   |-- ut_shape.h
|   |-- ut_rectangle.h
|   |-- ut_triangle.h
|   |-- ut_two_dimensional_vector.h
```
## Rivision history : v1.0
