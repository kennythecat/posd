#ifndef RECTANGLE_H
#define RECTANGLE_H

#include "shape.h"

class Rectangle: public Shape {
   public:
    Rectangle(double length, double width) : _length(length),_width(width)
    {
        _width  = width;
        _length = length;

        if((_width<=0)||(_length<=0))
        {
            string error="Width and Length should be positive.";
            throw error;
        }
        
    }
   
    double width()  const 
    { 
        return _width;  
    }
    double length() const 
    {
        return _length; 
    }
    double area() const override 
    { 
        return _length * _width; 
    }

    double perimeter() const override
    { 
        return (_width+_length)*2;
    }
    
    string info() const override
    { 
       double length =round(_length*100);
       length=length/100;

       double width =round(_width*100);
       width=width/100;

       stringstream ss;
       ss<<"Rectangle ("<<std::fixed<<std::setprecision(2)<<length<<" "<<width<<")";
       return ss.str();
    }


   private:
    double _length;
    double _width;
};



#endif