#ifndef SHAPE_FACTORY_H
#define SHAPE_FACTORY_H

#include "shape.h"
#include "circle.h"
#include "rectangle.h"
#include "triangle.h"
#include "two_dimensional_vector.h"
#include <stdexcept>

class AbstractShapeFactory {
public:
    virtual Shape* createCircle(double a) = 0;
    virtual Shape* createRectangle(double a, double b) = 0;
    virtual Shape* createTriangle(TwoDimensionalVector vec1, TwoDimensionalVector vec2) = 0;
};

class ShapeFactory : public AbstractShapeFactory {
public:
    Shape* createCircle(double a) override {
        return new Circle(a);
    }

    Shape* createRectangle(double a, double b) override {
        return new Rectangle(a, b);
    }

    Shape* createTriangle(TwoDimensionalVector vec1, TwoDimensionalVector vec2) override {
        return new Triangle(vec1, vec2);
    }
};

class NarrowShapeFactory : public AbstractShapeFactory {
public:
    Shape* createCircle(double a) override {
        throw std::invalid_argument("Invalid shape type.");
    }

    Shape* createRectangle(double a, double b) override {
        if(b>a){
            return new Rectangle(a, b);
        }
        else{
            return new Rectangle(b, a);
        }
        
    }

    Shape* createTriangle(TwoDimensionalVector vec1, TwoDimensionalVector vec2) override {
        if (vec1.x() > vec2.x()) {
            return new Triangle(vec1, vec2);  
        }
        else{
            return new Triangle(vec2, vec1);
        }
    }
};

class WideShapeFactory : public AbstractShapeFactory {
public:
    Shape* createCircle(double a) override {
        throw std::invalid_argument("Invalid shape type.");
    }
    Shape* createRectangle(double a, double b) override {
    if(a>b){
            return new Rectangle(a, b);
        }
        else{
            return new Rectangle(b, a);
        }
    }
    Shape* createTriangle(TwoDimensionalVector vec1, TwoDimensionalVector vec2) override {
        if (vec2.x() > vec1.x()) {
            return new Triangle(vec1, vec2);  
        }
        else{
            return new Triangle(vec2, vec1);
        }
    }
};

#endif
