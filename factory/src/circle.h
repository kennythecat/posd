#ifndef CIRCLE_H
#define CIRCLE_H
#include "shape.h"
#include <cmath>

class Circle:  public Shape {
    public:
        Circle(double radius): _radius(radius)
        {
            _radius=radius;
            if(_radius <= 0)
            {
                string error="Radius should be positive.";
                throw error;
            };

        }

        double area() const override 
        {
            return pow(_radius, 2) * M_PI;
        }
        
        double perimeter() const override 
        { 
            return _radius*2*M_PI;
        }

        string info() const override 
        { 

           double radius =round(_radius*100);
           radius =radius/100;
           stringstream ss;
           ss << "Circle ("<<fixed<<setprecision(2)<<radius<<")";
           
           return ss.str();
        }
        


    private:
        double _radius;
};

#endif