#include "../src/circle.h"
#include <string>

TEST(CaseCircle,RadiusShouldNotBeZero)
{
    try
    {
       Circle c(0.0);
    }
    catch(std::string s)
    {
        std::cerr << s << '\n';
    }
  
    ASSERT_ANY_THROW(Circle c(0.0));

}
TEST(CaseCircle,RadiusShouldNotBeNegative)
{
 
    ASSERT_ANY_THROW(Circle c(-1.0));

}
TEST(CaseCircle,NoException)
{
  
    ASSERT_NO_THROW(Circle c(2.0));


}

TEST(CaseCircle, Area) {
    Circle c(10.0);


    ASSERT_NEAR(314.159, c.area(), 0.001);

}
TEST(CaseCircle, Perimeter)
{
    Circle c(10.0);

    ASSERT_NEAR(62.831,c.perimeter(),0.001);


}
TEST(CaseCircle, Info)
{
    Circle c(10.001);
    ASSERT_STRCASEEQ("Circle (10.00)",(c.info()).c_str());
}
TEST(CaseCircle, Info1)
{
    Circle c(10);
    ASSERT_STRCASEEQ("Circle (10.00)",(c.info()).c_str());
}
TEST(CaseCircle, Info2)
{
    Circle c(10.355);
    ASSERT_STRCASEEQ("Circle (10.36)",(c.info()).c_str());
}