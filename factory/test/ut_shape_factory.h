#include "../src/shape_factory.h"
#include <stdexcept>

TEST(FactoryTest, CreateNarrowRectangle) {
    NarrowShapeFactory factory;
    Shape* shape = factory.createRectangle(10, 6);
    ASSERT_EQ("Rectangle (6.00 10.00)", shape->info()); 
    delete shape;
}

TEST(FactoryTest, CreateWideRectangle) {
    WideShapeFactory factory;
    Shape* shape = factory.createRectangle(12, 5);
    ASSERT_EQ("Rectangle (12.00 5.00)", shape->info()); 
    delete shape;
}

TEST(FactoryTest, CreateNarrowTriangle) {
    NarrowShapeFactory factory;
    TwoDimensionalVector v1(0.0, 2.0);
    TwoDimensionalVector v2(2.0, 0.0); 

    Shape* shape = factory.createTriangle( v1, v2);
    ASSERT_STRCASEEQ("Triangle ([2.00,0.00] [0.00,2.00])", (shape->info()).c_str()); 
    delete shape;
}

TEST(FactoryTest, CreateWideTriangle) {
    WideShapeFactory factory;
    TwoDimensionalVector v1(50.0, 0.0);
    TwoDimensionalVector v2(20.0, 2.0); 

    Shape* shape = factory.createTriangle(v1, v2);
    ASSERT_STRCASEEQ("Triangle ([20.00,2.00] [50.00,0.00])", (shape->info()).c_str()); 
    delete shape;
}