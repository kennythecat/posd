#include "../src/two_dimensional_vector.h"
TEST(CaseTwoDimensionalVector, ConstructorNoException)
{
    ASSERT_NO_THROW(TwoDimensionalVector v(1.00, 2.00));
}
TEST(CaseTwoDimensionalVector, x)
{

    TwoDimensionalVector v1(2.0, 0.0);
    ASSERT_NEAR(2.0, v1.x(), 0.001);
}
TEST(CaseTwoDimensionalVector, y)
{

    TwoDimensionalVector v1(2.0, 0.0);
    ASSERT_NEAR(0.0, v1.y(), 0.001);
}
TEST(CaseTwoDimensionalVector, length)
{

    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);
    TwoDimensionalVector v3 = v1.subtract(v2);

    ASSERT_NEAR(2.0, v1.length(), 0.001);
}

TEST(CaseTwoDimensionalVector, dotproductOrthogonal)
{

    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);
    TwoDimensionalVector v3 = v1.subtract(v2);

    ASSERT_NEAR(0.0, v1.dot(v2), 0.001);
}
TEST(CaseTwoDimensionalVector, dotproductAcuteAngle)
{

    TwoDimensionalVector v1(1.0, 1.0);
    TwoDimensionalVector v2(0.0, 1.0);
    TwoDimensionalVector v3 = v1.subtract(v2);

    ASSERT_NEAR(1.0, v1.dot(v2), 0.001);
}
TEST(CaseTwoDimensionalVector, dotproductObtuseAngle)
{

    TwoDimensionalVector v1(0.0, -1.0);
    TwoDimensionalVector v2(1.0, 1.0);
    TwoDimensionalVector v3 = v1.subtract(v2);

    ASSERT_NEAR(-1.0, v1.dot(v2), 0.001);
}
TEST(CaseTwoDimensionalVector, crossproductParalle)
{

    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(2.0, 0.0);

    ASSERT_NEAR(0.0, v1.cross(v2), 0.001);
}
TEST(CaseTwoDimensionalVector, crossproductClockWise)
{

    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);

    ASSERT_NEAR(4.0, v1.cross(v2), 0.001);
}
TEST(CaseTwoDimensionalVector, crossproductCounterClockWise)
{

    TwoDimensionalVector v1(0.0, 2.0);
    TwoDimensionalVector v2(2.0, 0.0);

    ASSERT_NEAR(-4.0, v1.cross(v2), 0.001);
}
TEST(CaseTwoDimensionalVector, sub)
{

    TwoDimensionalVector v1(2.0, 0.0);
    TwoDimensionalVector v2(0.0, 2.0);
    TwoDimensionalVector v3 = v1.subtract(v2);

    ASSERT_NEAR(2.0, v3.x(), 0.001);
    ASSERT_NEAR(-2.0, v3.y(), 0.001);
}
TEST(CaseTwoDimensionalVector, Info)
{
    TwoDimensionalVector v1(2.0, 0.0);

    ASSERT_STRCASEEQ("[2.00,0.00]", (v1.info()).c_str());
}
