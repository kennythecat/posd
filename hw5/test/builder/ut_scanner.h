#include "../../src/builder/scanner.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;
TEST(Scanner, scan)
{
    // std::string str = "compound {\n Circle 1.0\n circle 2.0\n }";
    // Scanner scan(str);

    // string myText;
    // ifstream MyReadFile("test/data/circle.txt");
    // while (getline(MyReadFile, myText))
    // {
    //     cout << myText << endl;
    // }
    // MyReadFile.close();
    // // cout << scan.next() << "<-Here";
    // // ASSERT_EQ("hi", scan.next());
    std::string input = "I Circle eee ,tt{t3.14159a";
    Scanner scanner(input);
    std::string first = scanner.next();  // `first` is "Circle"
    std::string second = scanner.next(); // `second` is ","
    std::string third = scanner.next();  // `second` is ","

    double forth = scanner.nextDouble(); // `third` is 3.14159
    ASSERT_EQ("Circle", first);
    ASSERT_EQ(",", second);
    ASSERT_EQ("{", third);
    ASSERT_EQ(3.14159, forth);
}
TEST(Scanner, isDone)
{
    std::string input = "i Circle ";
    Scanner scanner(input);
    cout << scanner.next() << endl; // `first` is "Circle"
    cout << scanner.next() << endl; // `first` is "Circle"

    ASSERT_TRUE(scanner.isDone());
}