#include <string>

#include "../../src/shape.h"
#include "../../src/builder/shape_builder.h"

TEST(CaseBuilder, BuildCircle)
{
    ShapeBuilder *builder = new ShapeBuilder();
    double radius = 1.0;
    builder->buildCircle(radius);
    Shape *result = builder->getShape();

    ASSERT_NEAR(1 * 1 * M_PI, result->area(), 0.001);

    delete builder;
}
TEST(CaseBuilder, BuildTRiange)
{
    ShapeBuilder *builder = new ShapeBuilder();
    double radius = 1.0;
    builder->buildTriangle(0.0, 2.0, 2.0, 0.0);
    Shape *result = builder->getShape();

    ASSERT_NEAR(2, result->area(), 0.001);

    delete builder;
}

TEST(CaseBuilder, ConstructRectangleAndCircle)
{
    ShapeBuilder builder;
    Director director(&builder);
    
    double length = 2.0;
    double width = 3.0;
    double radius = 1.0;

    director.constructRectangleAndCircle(length, width, radius);
    
    Shape *circle = builder.releaseShape();
    ASSERT_EQ(typeid(*circle), typeid(Circle));
    ASSERT_NEAR(radius * radius * M_PI, circle->area(), 0.001);
    delete circle;

    Shape *rectangle = builder.releaseShape();
    ASSERT_EQ(typeid(*rectangle), typeid(Rectangle));
    ASSERT_NEAR(length * width, rectangle->area(), 0.001);
    delete rectangle;
}

