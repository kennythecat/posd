#include <iostream>
#include "../../src/compound_shape.h"
#include "../../src/visitor/shape_info_visitor.h"

TEST(CaseShapeInfoVisitor, VisitCircle)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(10);
    cs1->addShape(c1);
    ShapeInfoVisitor visitor;
    cs1->accept(&visitor);
    ASSERT_EQ("CompoundShape{\n  Circle (10.00)\n}\n", visitor.getResult());
    delete cs1, c1;
}

TEST(CaseShapeInfoVisitor, VisitRectangle)
{
    CompoundShape *cs1 = new CompoundShape();
    Rectangle *r1 = new Rectangle(2, 2);
    cs1->addShape(r1);
    ShapeInfoVisitor visitor;
    cs1->accept(&visitor);
    ASSERT_EQ("CompoundShape{\n  Rectangle (2.00 2.00)\n}\n", visitor.getResult());
    delete cs1, r1;
}

TEST(CaseShapeInfoVisitor, VisitTriangle)
{
    TwoDimensionalVector v1(2, 0);
    TwoDimensionalVector v2(0, 2);
    CompoundShape *cs1 = new CompoundShape();
    Triangle *t1 = new Triangle(v1, v2);
    cs1->addShape(t1);
    ShapeInfoVisitor visitor;
    cs1->accept(&visitor);
    ASSERT_EQ("CompoundShape{\n  Triangle ([2.00,0.00] [0.00,2.00])\n}\n", visitor.getResult());
    delete cs1, t1;
}
TEST(CaseShapeInfoVisitor, VisitCompound)
{
    CompoundShape *cs1 = new CompoundShape();
    Circle *c1 = new Circle(1.1);
    Circle *c2 = new Circle(12.3456);
    Rectangle *r1 = new Rectangle(3.14, 4);
    cs1->addShape(c1);
    cs1->addShape(r1);

    CompoundShape *cs2 = new CompoundShape();
    cs2->addShape(c2);
    cs2->addShape(cs1);

    ShapeInfoVisitor visitor;
    cs2->accept(&visitor);
    // cout << typeid(cs1).name();
    // cout << visitor.getResult();

    ASSERT_EQ("CompoundShape{\n  Circle (12.35)\n  CompoundShape{\n    Circle (1.10)\n    Rectangle (3.14 4.00)\n  }\n}\n", visitor.getResult());
    delete c1, c2, r1;
}