#pragma once

#include "../compound_shape.h"
#include <stack>

class ShapeBuilder
{
public:
    void buildCircle(double radius)
    {
        _shapes.push(new Circle(radius));
    }
    void buildRectangle(double length, double width)
    {
        _shapes.push(new Rectangle(length, width));
    }
    void buildTriangle(double x1, double y1, double x2, double y2)
    {
        TwoDimensionalVector v1(x1, x2);
        TwoDimensionalVector v2(x2, y2);
        _shapes.push(new Triangle(v1, v2));
    }

    void buildCompoundBegin()
    {
        _shapes.push(new CompoundShape());
    }

    void buildCompoundEnd()
    {
        std::list<Shape *> components;
        while (typeid(*_shapes.top()) != typeid(CompoundShape) ||
               (!_shapes.top()->createIterator()->isDone() &&
                typeid(*_shapes.top()) == typeid(CompoundShape)))
        {
            components.push_back(_shapes.top());
            _shapes.pop();
        }
        Shape *compound = _shapes.top();
        for (auto it = components.rbegin(); it != components.rend(); it++)
        {
            compound->addShape(*it);
        }
    }

    Shape *getShape()
    {
        return _shapes.top();
    }

    Shape* releaseShape() {
    if(_shapes.empty()) return nullptr;
    Shape* shape = _shapes.top();
    _shapes.pop();
    return shape;
    }

private:
    std::stack<Shape *> _shapes;
};

class Director {
    ShapeBuilder* builder;
public:
    Director(ShapeBuilder* sb) : builder(sb) {}

    void constructRectangleAndCircle(double length, double width, double radius) {
        builder->buildRectangle(length, width);
        builder->buildCircle(radius);
    }
    
};