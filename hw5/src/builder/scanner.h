#pragma once
using namespace std;
#include <string>

class Scanner
{
public:
    Scanner(std::string input) : _input(input) {}

    std::string t1()
    {
        return _input;
    }
    std::string next()
    {
        int counter = 0;
        std::string result = "";
        for (int i = 0; i <= _input.length(); i++)
        {
            for (auto token : tokenList)
            {
                skipWhiteSpace();
                if (_input.compare(pos + i, token.length(), token) == 0)
                {
                    pos = pos + token.length() + i;
                    result = token;
                    return result;
                }
            }
            if (i + pos >= _input.length())
            {
                break;
            }
        }
        return result;
    }
    void skipWhiteSpace()
    {
        while (_input[pos] == ' ' || _input[pos] == '\n' || _input[pos] == '\t')
        {
            pos++;
        }
    }

    double nextDouble()
    {
        std::string s = "";
        skipWhiteSpace();
        while (1)
        {
            if ((_input[pos] <= '9' && _input[pos] >= '0') || _input[pos] == '.')
            {
                s = s + _input[pos];
            }
            else if (pos == _input.length())
            {
                break;
            }
            pos++;
        }
        return std::stod(s);
    }

    bool isDone()
    {
        skipWhiteSpace();
        cout << pos << "::" << _input.length() << endl;
        return pos == _input.length();
    }

private:
    int count = 0;
    bool run = true;
    std::string _input;
    std::string::size_type pos = 0;
    const std::vector<std::string> tokenList = {"Circle", "Rectangle", "Triangle", "CompoundShape", "(", ")", "[", "]", "{", "}", ","};
};