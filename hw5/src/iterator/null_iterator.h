#ifndef NULL_ITERATOR_H
#define NULL_ITERATOR_H

#include "iterator.h"

class NullIterator : public Iterator {
public:
    void first() override 
    { 
        std::string error="Error.";
        throw error;
    }

    Shape* currentItem() const override 
    { 
        string error="Error.";
        throw error;
    }

    void next() override 
    { 
        string error="Error.";
        throw error;
    }

    bool isDone() const override 
    { 
        return true;
    }
};

#endif